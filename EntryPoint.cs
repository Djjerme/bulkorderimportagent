using System;
using System.Xml;  
using System.Configuration; 
using SD = System.Diagnostics;
using SIO = System.IO;

namespace BulkOrderAgent
{

	class EntryPoint
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main(string[] args)
		{

			// Start log writer (append text to log)
			SIO.FileStream log = new SIO.FileStream(System.AppDomain.CurrentDomain.BaseDirectory + "ImportAgent_Log.txt", SIO.FileMode.Append) ;
			SD.TextWriterTraceListener logger = new SD.TextWriterTraceListener(log) ;
			SD.Trace.Listeners.Add(logger);
			SD.Trace.WriteLine("\r\n" + DateTime.Now + " Application execution started");

			// Check for parameters
			if (args.Length == 1)
			{
				try
				{

                    ImportAgent newImportAgent = new ImportAgent();
                    string path = SIO.Directory.GetCurrentDirectory() + "\\" + args[0].ToString();

                    Console.WriteLine("Starting session with settings from file '" + path + "'");
                    SD.Trace.WriteLine(DateTime.Now + " Starting session with settings from file '" + path + "'");

                    // 1. Load data from settings file
					newImportAgent.LoadSettingsFile(path);

                    // 2. Cycle through each of the files in the import directory					
                    SIO.DirectoryInfo di = new SIO.DirectoryInfo(newImportAgent.IncomingFileDirectory);
                    SIO.FileInfo[] files = di.GetFiles("*." + newImportAgent.IncomingFileExtension);

                    if (files.Length == 0)
                    {
                        Console.WriteLine("There are no files to be processed.");
                        SD.Trace.WriteLine(DateTime.Now + " There are no files to be processed.");
                    }
                    else
                    {

                        foreach (SIO.FileInfo fi in files)
                        {

                            Console.WriteLine("\r\nStarted processing import file'" + fi.Name + "'");
                            SD.Trace.WriteLine(DateTime.Now + " Started processing import file '" + fi.Name + "'");

                            // 3. Check whether file contains all required columns
                            // 4. Load data from file and convert to XML for storing in database
                            newImportAgent.LoadImportFileData(fi);

                            // 5. Store original file data in database
                            newImportAgent.StoreImportFileData();

                            // 6. Transform data according to transformation settings (CSV only)
                            // 7. Convert dataset to XML (CSV only)
                            newImportAgent.TransformImportFileData();

                            // 8. Verify data in XML checking for invalid or incomplete orders and invalid items, 
                            newImportAgent.VerifyImportFileData();

                            // 9. Export data to SQL server for bulk data processing by a stored procedure
                            newImportAgent.ImportFileData();

                            // 10. Get final operation details and send operation e-mails, if necessary
                            newImportAgent.FinishImport();

                            // 11. Send reorder notifications
                            newImportAgent.ProcessReorderNotifications();

                            Console.WriteLine("Finished processing import file'" + fi.Name + "'");
                            SD.Trace.WriteLine(DateTime.Now + " Finished processing import file '" + fi.Name + "'");

                        }

                    }

				}
				catch(Exception ex)
				{
                    SD.Trace.WriteLine(DateTime.Now + " Execution Stopped. Program has encountered an error: " + ex.Message.ToString() + " Stack Trace: " + ex.StackTrace.ToString());
					Console.WriteLine("\nExecution Stopped. Program has encountered an error: " + ex.Message.ToString() + " Stack Trace: " + ex.StackTrace.ToString());
				}
			} 
			else 
			{
				SD.Trace.WriteLine(DateTime.Now + " Execution Stopped. Please provide a path and file name of the configuration file. Do not include spaces or additional parameters.\n\n EXAMPLE USAGE: BulkOrderImportAgent.exe c:\\outputFilePath\\outputFileName.xml");
                Console.WriteLine("\nExecution Stopped. Please provide a path and file name of the configuration file. Do not include spaces or additional parameters.\n\n EXAMPLE USAGE: BulkOrderImportAgent.exe c:\\outputFilePath\\outputFileName.xml");

			}

			// Close log writer
            Console.WriteLine("Operation completed - see log file for details");
			SD.Trace.WriteLine(DateTime.Now + " Application execution completed.");
			SD.Trace.Close();

		}
	}
}
