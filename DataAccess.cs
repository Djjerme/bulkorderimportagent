using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace BulkOrderAgent
{
    class DataAccess
    {

        public bool CheckDbValue(string dbConnection, string dbStoredProcedure, int storeId, string valueToCheck, string valueParameter)
        {

            using (SqlConnection sqlConn = new SqlConnection(dbConnection))
            {
                sqlConn.Open();

                SqlCommand sqlCmd = sqlConn.CreateCommand();
                SqlDataAdapter daResults = new SqlDataAdapter();
                bool flagResult = false;

                sqlCmd.CommandTimeout = 900;
                sqlCmd.Connection = sqlConn;

                try
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = dbStoredProcedure;

                    SqlParameter paramStoreId = sqlCmd.Parameters.Add("@iStoreId", SqlDbType.Int);
                    SqlParameter paramCheckValue = sqlCmd.Parameters.Add("@sValue", SqlDbType.VarChar,255);
                    SqlParameter paramCheckParameter = sqlCmd.Parameters.Add("@sParameter", SqlDbType.VarChar, 255);
                    SqlParameter paramCheckResult = sqlCmd.Parameters.Add("@bResult", SqlDbType.Bit);
                    paramStoreId.Value = storeId;
                    paramCheckValue.Value = valueToCheck;
                    paramCheckParameter.Value = valueParameter;
                    paramCheckResult.Direction = ParameterDirection.Output;

                    sqlCmd.ExecuteNonQuery();

                    flagResult = System.Convert.ToBoolean(paramCheckResult.Value);

                    return flagResult;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


        }


        public DataSet ProcessXmlFileDataWithResults(int storeId, string dbConnection, string dbStoredProcedure, XmlDocument xmlDoc)
        {

            // Call stored procedure and pass XML document to it
            using (SqlConnection sqlConn = new SqlConnection(dbConnection))
            {
                sqlConn.Open();

                SqlCommand sqlCmd = sqlConn.CreateCommand();
                SqlDataAdapter daResults = new SqlDataAdapter();
                DataSet dsResults = new DataSet();

                sqlCmd.CommandTimeout = 3600;
                sqlCmd.Connection = sqlConn;

                try
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = dbStoredProcedure;

                    SqlParameter paramStoreId = sqlCmd.Parameters.Add("@iStoreId", SqlDbType.Int);
                    SqlParameter paramXMLData = sqlCmd.Parameters.Add("@sDataXML", SqlDbType.NText);
                    paramStoreId.Value = storeId;
                    paramXMLData.Value = xmlDoc.OuterXml;

                    daResults.SelectCommand = sqlCmd;
                    daResults.Fill(dsResults);

                    return dsResults;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }

        public void ProcessXmlFileData(int storeId, string dbConnection, string dbStoredProcedure, XmlDocument xmlDoc)
        {

            // Call stored procedure and pass xml document to it
            using (SqlConnection sqlConn = new SqlConnection(dbConnection))
            {
                sqlConn.Open();

                SqlCommand sqlCmd = sqlConn.CreateCommand();
                SqlDataAdapter daResults = new SqlDataAdapter();

                sqlCmd.CommandTimeout = 900;
                sqlCmd.Connection = sqlConn;

                try
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = dbStoredProcedure;

                    SqlParameter paramStoreId = sqlCmd.Parameters.Add("@iStoreId", SqlDbType.Int);
                    SqlParameter paramXMLData = sqlCmd.Parameters.Add("@sDataXML", SqlDbType.NText);
                    paramStoreId.Value = storeId;
                    paramXMLData.Value = xmlDoc.OuterXml;

                    sqlCmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }


    }
}
