using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Net.Mail;
using SD = System.Diagnostics;
using System.Xml;
using System.IO;
using System.Reflection;

namespace BulkOrderAgent
{

    class Mailer
    {

        bool flagEmailSent;

        public bool EmailSent
        {
            get { return flagEmailSent; }
        }

        // Sends e-mail message
        public void SendMail(string mailServer, string mailFromAddress, string mailToList, string mailCcList, string mailBccList, string mailSubject, string mailPriority, string mailBody, bool mailHtmlEncodingFlag, string mailArchivePath, bool mailArchiveFlag)
        {

            SmtpClient smtpClient = new SmtpClient();
            MailMessage mailMessage = new MailMessage();

            mailMessage.From = new MailAddress(mailFromAddress);

            // Add mail recipients
            if (mailToList != String.Empty)
            {

                string[] mailToRecipients = mailToList.Split(new Char[] { ';' });

                foreach (string recipient in mailToRecipients)
                {
                    if (recipient.Trim() != String.Empty)
                    {
                        mailMessage.To.Add(new MailAddress(recipient.Trim()));
                    }
                }

            }

            // Add CC recipients
            if (mailCcList != String.Empty)
            {

                string[] mailCcRecipients = mailCcList.Split(new Char[] { ';' });

                foreach (string recipient in mailCcRecipients)
                {
                    if (recipient.Trim() != String.Empty)
                    {
                        mailMessage.CC.Add(new MailAddress(recipient.Trim()));
                    }
                }
            }

            // Add BCC recipients
            if (mailBccList != String.Empty)
            {

                string[] mailBccRecipients = mailBccList.Split(new Char[] { ';' });

                foreach (string recipient in mailBccRecipients)
                {
                    if (recipient.Trim() != String.Empty)
                    {
                        mailMessage.Bcc.Add(new MailAddress(recipient.Trim()));
                    }
                }

            }

            mailMessage.Subject = mailSubject;
            mailMessage.Body = mailBody;
            mailMessage.IsBodyHtml = mailHtmlEncodingFlag;

            try
            {
                if (mailArchiveFlag)
                {
                    if (!String.IsNullOrEmpty(mailArchivePath))
                    {
                        SaveMail(mailMessage, mailArchivePath);
                    }
                }
            }
            catch { }

            try
            {
                smtpClient.Host = mailServer;
                smtpClient.Send(mailMessage);
                flagEmailSent = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred when attempting to send an e-mail." + ex.GetType() + ". Message: " + ex.Message);
                SD.Trace.WriteLine(DateTime.Now + " Exception occurred when attempting to send an e-mail. Exception Type: " + ex.GetType() + ". Message: " + ex.Message);
                flagEmailSent = false;
            }

        }

        public void SaveMail(MailMessage Message, string FileName)
        {
            Assembly assembly = typeof(SmtpClient).Assembly;
            Type _mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");

            String dt = DateTime.Now.ToString("MMddyyyyHmm");

            FileName = FileName + "sent_email_" + dt + ".eml";

            using (FileStream _fileStream = new FileStream(FileName, FileMode.Create))
            {
                // Get reflection info for MailWriter contructor
                ConstructorInfo _mailWriterContructor =
                    _mailWriterType.GetConstructor(
                        BindingFlags.Instance | BindingFlags.NonPublic,
                        null,
                        new Type[] { typeof(Stream) },
                        null);

                // Construct MailWriter object with our FileStream
                object _mailWriter = _mailWriterContructor.Invoke(new object[] { _fileStream });

                // Get reflection info for Send() method on MailMessage
                MethodInfo _sendMethod =
                    typeof(MailMessage).GetMethod(
                        "Send",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call method passing in MailWriter
                _sendMethod.Invoke(
                    Message,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { _mailWriter, true },
                    null);

                // Finally get reflection info for Close() method on our MailWriter
                MethodInfo _closeMethod =
                    _mailWriter.GetType().GetMethod(
                        "Close",
                        BindingFlags.Instance | BindingFlags.NonPublic);

                // Call close method
                _closeMethod.Invoke(
                    _mailWriter,
                    BindingFlags.Instance | BindingFlags.NonPublic,
                    null,
                    new object[] { },
                    null);
            }
        }


    }


}
