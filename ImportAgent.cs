using System;
using System.Xml;  
using System.Data;
using System.Data.SqlClient; 
using System.IO;
using SD = System.Diagnostics;

namespace BulkOrderAgent
{

	class ImportAgent
    {

        #region "Fields"

        // Fields to store job settings
		string	mailServerAddress;
		string	databaseConnectionString;

		string	incomingFileDirectory;
		string	incomingFileExtension;
		bool	incomingFileHeaders;
		string	archiveFilePath;
        string  archiveXmlPath;
        string archiveEmailPath;

		string	incomingRowDelimiter;
		string	incomingColDelimiter;

        bool    flagArchiveFile;
		bool	flagKeepFile;
        bool    flagKeepOrderDate;
        bool    flagResumeOnOrderError;
        bool    flagResumeOnItemError;
        bool    flagStoreFileContents;
        bool    flagImportFileContents;
        bool    flagValidateLineItems;
        bool flagArchiveEmail;
    
        string  sqlSPValidateOrder;
        string  sqlSPValidateItem;
        bool    flagXmlArchive;

        bool    flagEmailOrderConfirmation;
        bool    flagEmailAdminConfirmation;
        bool    flagEmailReorderNotification;

        string  emailOrderConfirmFromAddress;
		string	emailOrderConfirmSubject;
        string  emailOrderConfirmBody;
        string  emailOrderConfirmRecipientList;
        string  emailOrderConfirmCCList;
        string  emailOrderConfirmBCCList;
        string  emailOrderConfirmPriority;

        string  emailAdminConfirmFromAddress;
        string  emailAdminConfirmSubject;
        string  emailAdminConfirmBody;
        string  emailAdminConfirmRecipientList;
        string  emailAdminConfirmCCList;
        string  emailAdminConfirmBCCList;
        string  emailAdminConfirmPriority;

        string emailAdminIncompleteFromAddress;
        string emailAdminIncompleteSubject;
        string emailAdminIncompleteBody;
        string emailAdminIncompleteRecipientList;
        string emailAdminIncompleteCCList;
        string emailAdminIncompleteBCCList;
        string emailAdminIncompletePriority;

        string  emailAdminErrorFromAddress;
        string  emailAdminErrorSubject;
        string  emailAdminErrorBody;
        string  emailAdminErrorRecipientList;
        string  emailAdminErrorCCList;
        string  emailAdminErrorBCCList;
        string  emailAdminErrorPriority;

        string emailReorderNotificationFromAddress;
        string emailReorderNotificationSubject;
        string emailReorderNotificationBody;
        string emailReorderNotificationRecipientList;
        string emailReorderNotificationCCList;
        string emailReorderNotificationBCCList;
        string emailReorderNotificationPriority;

		//Fields to store import file data
        FileInfo currentImportFileInfo;
		int		storeId;
        int     importFileId;
        bool    flagValidImportFile;
		DataSet dsImportFile;

        // Dataset to skipped order data from the original file
        DataSet dsUnprocessedOrders;
        
        // Dataset to store final order data returned from database
        DataSet dsProcessedOrders;

        // XML representation of raw data loaded from the original file
        XmlDocument xmlLoadedDataDoc;

        // XML representation of file information data from the original file
        XmlDocument xmlFileDataDoc;

        // XML representation of validation errors for data from the original file
        XmlDocument xmlFileValidationErrorDoc;

        // XML representation of transformed and later verified import data
        XmlDocument xmlImportDataDoc;


		// Fields to store column numbers used for data transformation
        int columnOrderExternalId;
		int columnOrderDate;
        int columnOrderStatus;
        int columnOrderType;
        int columnExternalOrderType;
        int columnExternalOrderFlag;
        int columnCustomerOrderNo;
        int columnReferralId;
        int columnProjectNo;
        int columnOrderSource;

        int columnOrderTotal;
        int columnOrderShippingCharge;
        int columnOrderHandlingCharge;
        int columnSalesTax;
        int columnSalesTaxId;
        int columnSalesTaxCounty;

        int columnShipMethod;
        int columnNeedDate;
        int columnOrderReason;
        int columnShipInstructions;
        int columnShipLabel;
        int columnWhseLabel;
        int columnCostCenter;

        int columnThirdPartyShipFlag;
        int columnThirdPatryShipInfo;
        int columnShipInvoiceTemplate;
        int columnShipAttnTo;
        int columnCustomMessage;
        
		int columnBillToFirstName;
		int columnBillToLastName;
		int columnBillToAddress1;
		int columnBillToAddress2;
		int columnBillToCity;
		int columnBillToState;
		int columnBillToZip;
		int columnBillToCountry;
		int columnBillToPhone;
		int columnBillToEmail;
		int columnBillToCompany;

        int columnShipToFirstName;
        int columnShipToLastName;
        int columnShipToAddress1;
        int columnShipToAddress2;
        int columnShipToCity;
        int columnShipToState;
        int columnShipToZip;
        int columnShipToCountry;
        int columnShipToPhone;
        int columnShipToEmail;
        int columnShipToCompany;

		int columnItemLineNumber;
		int columnItemID;
		int columnItemDescription;
		int columnItemCustom1;
		int columnItemCustom2;
		int columnItemCustom3;
		int columnItemUnitPrice;
		int columnItemTax;
		int columnItemShippingCharge;
		int columnItemQuantity;

        #endregion

        #region "Constructors"

        public ImportAgent()
		{

			mailServerAddress = "";
			databaseConnectionString = "";

			incomingFileDirectory = "";
			incomingFileExtension = "";
			incomingFileHeaders = false;
			archiveFilePath = "";
            archiveXmlPath = "";
            archiveEmailPath = "";

			incomingRowDelimiter = "";
			incomingColDelimiter = "";

            flagArchiveFile = false;
			flagKeepFile = false;
            flagKeepOrderDate = false;
            flagXmlArchive = false;
            flagEmailOrderConfirmation = false;
            flagResumeOnOrderError = false;
            flagResumeOnItemError = false;
            flagStoreFileContents = false;
            flagImportFileContents = false;
            flagValidateLineItems = true;
            flagArchiveEmail = false;

            sqlSPValidateOrder = String.Empty;
            sqlSPValidateItem = String.Empty;

            importFileId = 0;
            flagValidImportFile = true;

            flagEmailOrderConfirmation = false;
            flagEmailAdminConfirmation = false;
            flagEmailReorderNotification = false;

            emailOrderConfirmFromAddress = String.Empty;
            emailOrderConfirmSubject = String.Empty;
            emailOrderConfirmBody = String.Empty;
            emailOrderConfirmRecipientList = String.Empty;
            emailOrderConfirmCCList = String.Empty;
            emailOrderConfirmBCCList = String.Empty;
            emailOrderConfirmPriority = String.Empty;

            emailAdminConfirmFromAddress = String.Empty;
            emailAdminConfirmSubject = String.Empty;
            emailAdminConfirmBody = String.Empty;
            emailAdminConfirmRecipientList = String.Empty;
            emailAdminConfirmCCList = String.Empty;
            emailAdminConfirmBCCList = String.Empty;
            emailAdminConfirmPriority = String.Empty;

            emailAdminIncompleteFromAddress = String.Empty;
            emailAdminIncompleteSubject = String.Empty;
            emailAdminIncompleteBody = String.Empty;
            emailAdminIncompleteRecipientList = String.Empty;
            emailAdminIncompleteCCList = String.Empty;
            emailAdminIncompleteBCCList = String.Empty;
            emailAdminIncompletePriority = String.Empty;

            emailAdminErrorFromAddress = String.Empty;
            emailAdminErrorSubject = String.Empty;
            emailAdminErrorBody = String.Empty;
            emailAdminErrorRecipientList = String.Empty;
            emailAdminErrorCCList = String.Empty;
            emailAdminErrorBCCList = String.Empty;
            emailAdminErrorPriority = String.Empty;

            emailReorderNotificationFromAddress = String.Empty;
            emailReorderNotificationSubject = String.Empty;
            emailReorderNotificationBody = String.Empty;
            emailReorderNotificationRecipientList = String.Empty;
            emailReorderNotificationCCList = String.Empty;
            emailReorderNotificationBCCList = String.Empty;
            emailReorderNotificationPriority = String.Empty;

            xmlLoadedDataDoc = new XmlDocument();
            xmlFileDataDoc = new XmlDocument();
            xmlFileValidationErrorDoc = new XmlDocument();
            xmlImportDataDoc = new XmlDocument();

			dsImportFile = new DataSet();
            dsProcessedOrders = new DataSet();
            
            dsUnprocessedOrders = new DataSet();
            dsUnprocessedOrders.Tables.Add("Orders");
            dsUnprocessedOrders.Tables["Orders"].Columns.Add("OrderID");
            dsUnprocessedOrders.Tables["Orders"].Columns.Add("LineItemNo");
            dsUnprocessedOrders.Tables["Orders"].Columns.Add("ItemID");
            dsUnprocessedOrders.Tables["Orders"].Columns.Add("ErrorCode");
            dsUnprocessedOrders.Tables["Orders"].Columns.Add("ErrorDesc");

            columnOrderExternalId = -1;
            columnOrderDate = -1;
            columnOrderStatus = -1;
            columnOrderType = -1;
            columnExternalOrderType = -1;
            columnExternalOrderFlag = -1;
            columnCustomerOrderNo = -1;
            columnReferralId = -1;
            columnProjectNo = -1;
            columnOrderSource = -1;

            columnOrderTotal = -1;
            columnOrderShippingCharge = -1;
            columnOrderHandlingCharge = -1;
            columnSalesTax = -1;
            columnSalesTaxId = -1;
            columnSalesTaxCounty = -1;

            columnShipMethod = -1;
            columnNeedDate = -1;
            columnOrderReason = -1;
            columnShipInstructions = -1;
            columnShipLabel = -1;
            columnWhseLabel = -1;
            columnCostCenter = -1;

            columnThirdPartyShipFlag = -1;
            columnThirdPatryShipInfo = -1;
            columnShipInvoiceTemplate = -1;
            columnShipAttnTo = -1;
            columnCustomMessage = -1;

            columnBillToFirstName = -1;
            columnBillToLastName = -1;
            columnBillToAddress1 = -1;
            columnBillToAddress2 = -1;
            columnBillToCity = -1;
            columnBillToState = -1;
            columnBillToZip = -1;
            columnBillToCountry = -1;
            columnBillToPhone = -1;
            columnBillToEmail = -1;
            columnBillToCompany = -1;

            columnShipToFirstName = -1;
            columnShipToLastName = -1;
            columnShipToAddress1 = -1;
            columnShipToAddress2 = -1;
            columnShipToCity = -1;
            columnShipToState = -1;
            columnShipToZip = -1;
            columnShipToCountry = -1;
            columnShipToPhone = -1;
            columnShipToEmail = -1;
            columnShipToCompany = -1;

            columnItemLineNumber = -1;
            columnItemID = -1;
            columnItemDescription = -1;
            columnItemCustom1 = -1;
            columnItemCustom2 = -1;
            columnItemCustom3 = -1;
            columnItemUnitPrice = -1;
            columnItemTax = -1;
            columnItemShippingCharge = -1;

		}


		#endregion

	#region "Properties"

        // Returns path of the order file folder
        public string IncomingFileDirectory
        {
           get { return incomingFileDirectory; }
        }

        // Returns extension of the order files
        public string IncomingFileExtension
        {
            get { return incomingFileExtension; }
        }

        public bool IsImportFileValid
        {
            get { return flagValidImportFile; }
        }

	#endregion

	#region "Methods"

		// 1. Load import settings, if settings file exists
		public void LoadSettingsFile(string settingsFilePath)
		{

			try
			{

				FileInfo fi = new FileInfo(settingsFilePath);

				if (fi.Exists) 
				{

                    XmlDocument settingsXmlDoc = new XmlDocument();
                    settingsXmlDoc.PreserveWhitespace = true;
                    settingsXmlDoc.Load(settingsFilePath);

                    LoadSettingsSession(settingsXmlDoc);
                    LoadSettingsMail(settingsXmlDoc);

                    if (incomingFileExtension.ToLower() != "xml")
                    {
                        LoadSettingsTransform(settingsXmlDoc);
                    }

				} 
				else 
				{
					throw new Exception("Settings file '" + settingsFilePath + "' does not exist."); 
				}

			}
			catch (Exception ex)
			{
                HandleException(ex.Message.ToString(), ex.StackTrace.ToString(), true, true);
			}

		}

		// Load general import session settings
		private void LoadSettingsSession(XmlDocument settingsXml)
		{

            string xmlValueCheck = String.Empty;
            string xmlValueLoadErrors = String.Empty;

			try
			{

                mailServerAddress = settingsXml.SelectSingleNode("//agentsettings/mailserver").InnerText;
                databaseConnectionString = settingsXml.SelectSingleNode("//agentsettings/dbconnection").InnerText;
                
                archiveFilePath = settingsXml.SelectSingleNode("//agentsettings/archive_file_dir").InnerText;
                archiveXmlPath = settingsXml.SelectSingleNode("//agentsettings/archive_xml_dir").InnerText;
                archiveEmailPath = settingsXml.SelectSingleNode("//agentsettings/archive_email_dir").InnerText;

                incomingFileDirectory = settingsXml.SelectSingleNode("//agentsettings/incoming_file_dir").InnerText;
                incomingFileExtension = settingsXml.SelectSingleNode("//agentsettings/incoming_file_extension").InnerText;
                incomingRowDelimiter = settingsXml.SelectSingleNode("//agentsettings/incoming_row_delimiter").InnerText;
                incomingColDelimiter = settingsXml.SelectSingleNode("//agentsettings/incoming_col_delimiter").InnerText;

                sqlSPValidateOrder = settingsXml.SelectSingleNode("//agentsettings/order_validator_sp").InnerText;
                sqlSPValidateItem = settingsXml.SelectSingleNode("//agentsettings/item_validator_sp").InnerText;

                storeId = System.Convert.ToInt16(settingsXml.SelectSingleNode("//agentsettings/store_id").InnerXml);
                
                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/archive_file_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagArchiveFile = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagArchiveFile = false; }
                else { xmlValueLoadErrors += "Value in 'archive_file_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/archive_file_keep_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagKeepFile = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagKeepFile = false; }
                else { xmlValueLoadErrors += "Value in 'archive_file_keep_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/archive_xml_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagXmlArchive = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagXmlArchive = false; }
                else { xmlValueLoadErrors += "Value in 'archive_xml_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/incoming_file_headers").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { incomingFileHeaders = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { incomingFileHeaders = false; }
                else { xmlValueLoadErrors += "Value in 'incoming_file_headers' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/incoming_file_keep_order_date").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagKeepOrderDate = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagKeepOrderDate = false; }
                else { xmlValueLoadErrors += "Value in 'incoming_file_keep_order_date' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/store_file_contents_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagStoreFileContents = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagStoreFileContents = false; }
                else { xmlValueLoadErrors += "Value in 'store_file_contents_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/import_file_contents_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagImportFileContents = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagImportFileContents = false; }
                else { xmlValueLoadErrors += "Value in 'import_file_contents_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/validate_line_items_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagValidateLineItems = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagValidateLineItems = false; }
                else { xmlValueLoadErrors += "Value in 'validate_line_items_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/resume_order_error_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagResumeOnOrderError = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagResumeOnOrderError = false; }
                else { xmlValueLoadErrors += "Value in 'resume_order_error_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }
                
                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/resume_item_error_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagResumeOnItemError = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagResumeOnItemError = false; }
                else { xmlValueLoadErrors += "Value in 'resume_item_error_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/email_order_confirmation_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagEmailOrderConfirmation = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagEmailOrderConfirmation = false; }
                else { xmlValueLoadErrors += "Value in 'email_order_confirmation_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/archive_email_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagArchiveEmail = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagArchiveEmail = false; }
                else { xmlValueLoadErrors += "Value in 'archive_email_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/email_import_confirmation_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagEmailAdminConfirmation = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagEmailAdminConfirmation = false; }
                else { xmlValueLoadErrors += "Value in 'email_import_confirmation_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                xmlValueCheck = settingsXml.SelectSingleNode("//agentsettings/email_reorder_flag").InnerText;
                if (xmlValueCheck == "true" || xmlValueCheck == "1" || xmlValueCheck == "yes") { flagEmailReorderNotification = true; }
                else if (xmlValueCheck == "false" || xmlValueCheck == "0" || xmlValueCheck == "no") { flagEmailReorderNotification = false; }
                else { xmlValueLoadErrors += "Value in 'email_reorder_flag' is invalid (only following are allowed: 0,1,true,false,yes,no)\n"; }

                if (xmlValueLoadErrors != String.Empty)
                {
                    throw new Exception(xmlValueLoadErrors);
                }

			} 
			catch (Exception ex)
			{
                if (xmlValueLoadErrors == String.Empty)
                {
                    throw new Exception("One or more elements in import session settings were not found. Please check the existance and spelling of all setting nodes. " + ex.Message);
                }
                else
                {
                    throw new Exception("Please check the existance and spelling of all setting nodes.\n" + ex.Message);
                }
			}

		}

        // Load e-mail notification settings
        private void LoadSettingsMail(XmlDocument settingsXml)
        {

            try
            {
                // Order Confirmation
                if (flagEmailOrderConfirmation == true)
                {
                    emailOrderConfirmFromAddress = settingsXml.SelectSingleNode("//agentsettings/email_order_confirmation_from").InnerText;
                    emailOrderConfirmSubject = settingsXml.SelectSingleNode("//agentsettings/email_order_confirmation_subject").InnerText;
                    emailOrderConfirmBody = settingsXml.SelectSingleNode("//agentsettings/email_order_confirmation_body").InnerText;
                    emailOrderConfirmCCList = settingsXml.SelectSingleNode("//agentsettings/email_order_confirmation_cc_list").InnerText;
                    emailOrderConfirmBCCList = settingsXml.SelectSingleNode("//agentsettings/email_order_confirmation_bcc_list").InnerText;
                }

                // Admin Import Summary/Confirmation
                if (flagEmailAdminConfirmation == true)
                {
                    emailAdminConfirmFromAddress = settingsXml.SelectSingleNode("//agentsettings/email_import_confirmation_from").InnerText;
                    emailAdminConfirmSubject = settingsXml.SelectSingleNode("//agentsettings/email_import_confirmation_subject").InnerText;
                    emailAdminConfirmBody = settingsXml.SelectSingleNode("//agentsettings/email_import_confirmation_body").InnerText;
                    emailAdminConfirmRecipientList = settingsXml.SelectSingleNode("//agentsettings/email_import_confirmation_recipient_list").InnerText;
                    emailAdminConfirmCCList = settingsXml.SelectSingleNode("//agentsettings/email_import_confirmation_cc_list").InnerText;
                    emailAdminConfirmBCCList = settingsXml.SelectSingleNode("//agentsettings/email_import_confirmation_bcc_list").InnerText;
                    emailAdminIncompleteFromAddress = settingsXml.SelectSingleNode("//agentsettings/email_import_incomplete_from").InnerText;
                    emailAdminIncompleteSubject = settingsXml.SelectSingleNode("//agentsettings/email_import_incomplete_subject").InnerText;
                    emailAdminIncompleteBody = settingsXml.SelectSingleNode("//agentsettings/email_import_incomplete_body").InnerText;
                    emailAdminIncompleteRecipientList = settingsXml.SelectSingleNode("//agentsettings/email_import_incomplete_recipient_list").InnerText;
                    emailAdminIncompleteCCList = settingsXml.SelectSingleNode("//agentsettings/email_import_incomplete_cc_list").InnerText;
                    emailAdminIncompleteBCCList = settingsXml.SelectSingleNode("//agentsettings/email_import_incomplete_bcc_list").InnerText;
                }

                // Admin Error Notification
                emailAdminErrorFromAddress = settingsXml.SelectSingleNode("//agentsettings/email_admin_error_from").InnerText;
                emailAdminErrorSubject = settingsXml.SelectSingleNode("//agentsettings/email_admin_error_subject").InnerText;
                emailAdminErrorBody = settingsXml.SelectSingleNode("//agentsettings/email_admin_error_body").InnerText;
                emailAdminErrorRecipientList = settingsXml.SelectSingleNode("//agentsettings/email_admin_error_recipient_list").InnerText;
                emailAdminErrorCCList = settingsXml.SelectSingleNode("//agentsettings/email_admin_error_cc_list").InnerText;
                emailAdminErrorBCCList = settingsXml.SelectSingleNode("//agentsettings/email_admin_error_bcc_list").InnerText;

                // Reorder Notification
                emailReorderNotificationFromAddress = settingsXml.SelectSingleNode("//agentsettings/email_reorder_from").InnerText;
                emailReorderNotificationSubject = settingsXml.SelectSingleNode("//agentsettings/email_reorder_subject").InnerText;
                emailReorderNotificationBody = settingsXml.SelectSingleNode("//agentsettings/email_reorder_body").InnerText;
                emailReorderNotificationRecipientList = settingsXml.SelectSingleNode("//agentsettings/email_reorder_recipient_list").InnerText;
                emailReorderNotificationCCList = settingsXml.SelectSingleNode("//agentsettings/email_reorder_cc_list").InnerText;
                emailReorderNotificationBCCList = settingsXml.SelectSingleNode("//agentsettings/email_reorder_bcc_list").InnerText;

            }
            catch (Exception ex)
            {
                HandleException("One or more elements in e-mail notification settings were not found. Please check the existance and spelling of all settings tags.", ex.Message + ' ' + ex.StackTrace, true, true);
            }

        }

        // Load column transformation settings for CSV files
        private void LoadSettingsTransform(XmlDocument settingsXml)
        {

            string xmlValueCheck = String.Empty;
            string xmlValueLoadErrors = String.Empty;

            try
            {

                XmlNode xmlRoot;
                xmlRoot = settingsXml.DocumentElement;

                foreach (XmlNode node in xmlRoot.ChildNodes)
                {
                    #region "Node Switch"
                    switch (node.Name)
                    {

                        case "import_column_order_id":
                            try { columnOrderExternalId = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Order ID' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_order_date":
                            try { columnOrderDate = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Order Date' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_status":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnOrderStatus = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnOrderStatus = -1; }
                            }
                            catch
                            {
                                throw new Exception("Unable to retrieve 'Order Status' numerical column designation. Please make sure the value is a numerical character. This column is optional.");
                            }
                            break;

                        case "import_column_order_type":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnOrderType = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnOrderType = -1; }
                            }
                            catch
                            {
                                throw new Exception("Unable to retrieve 'Order Type' numerical column designation. Please make sure the value is a numerical character. This column is optional.");
                            }
                            break;

                        case "import_column_external_order_type":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnExternalOrderType = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnExternalOrderType = -1; }
                            }
                            catch
                            {
                                throw new Exception("Unable to retrieve 'External Order Type' numerical column designation. Please make sure the value is a numerical character. This column is optional.");
                            }
                            break;

                        case "import_column_external_order_flag":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnExternalOrderFlag = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnExternalOrderFlag = -1; }
                            }
                            catch
                            {
                                throw new Exception("Unable to retrieve 'External Order Flag' numerical column designation. Please make sure the value is a numerical character. This column is optional.");
                            }
                            break;

                        case "import_column_order_cust_no":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnCustomerOrderNo = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnCustomerOrderNo = -1; }
                            }
                            catch
                            {
                                throw new Exception("Unable to retrieve 'Customer Order Number' numerical column designation. Please make sure the value is a numerical character. This column is optional.");
                            }
                            break;

                        case "import_column_order_referral_id":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnReferralId = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnReferralId = -1; }
                            }
                            catch
                            {
                                throw new Exception("Unable to retrieve 'Customer Referral Id' numerical column designation. Please make sure the value is a numerical character. This column is optional.");
                            }
                            break;

                        case "import_column_order_project_no":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnProjectNo = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnProjectNo = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Project Number' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_source":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnOrderSource = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnOrderSource = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Order Source' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_total":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnOrderTotal = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnOrderTotal = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Order Total' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_shipping_charge":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnOrderShippingCharge = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnOrderShippingCharge = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Order Ship Charge' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_handling_charge":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnOrderHandlingCharge = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnOrderHandlingCharge = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Order Handling Charge' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_sales_tax":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnSalesTax = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnSalesTax = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Sales Tax' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_sales_tax_id":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnSalesTaxId = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnSalesTaxId = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Sales Tax Id' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_sales_tax_county":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnSalesTaxCounty = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnSalesTaxCounty = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Sales Tax County' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_ship_method":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipMethod = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipMethod = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Requsted Ship Method' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_need_date":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnNeedDate = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnNeedDate = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Need By Date' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_order_reason":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnOrderReason = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnOrderReason = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Order Reason' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_shipping_instructions":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipInstructions = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipInstructions = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Shipping Instructions' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_shipping_label":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipLabel = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipLabel = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Shipping Label Comment' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_whse_label":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnWhseLabel = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnWhseLabel = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Warehouse Comment' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_cost_center":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnCostCenter = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnCostCenter = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Cost Center' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_third_party_shipper_flag":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnThirdPartyShipFlag = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnThirdPartyShipFlag = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Third Party Shipper Flag' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_third_party_shipper_info":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnThirdPatryShipInfo = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnThirdPatryShipInfo = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Third Party Shipper Info' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_ship_invoice_template_id":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipInvoiceTemplate = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipInvoiceTemplate = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Ship Invoice Template' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_ship_attn_to":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipAttnTo = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipAttnTo = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Ship Attention To' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_order_custom_message":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnCustomMessage = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnCustomMessage = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Custom Message' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_billing_firstname":
                            try { columnBillToFirstName = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Billing First Name' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_billing_lastname":
                            try
                            {
                                columnBillToLastName = System.Convert.ToInt32(node.InnerText);
                            }
                            catch
                            {
                                columnBillToLastName = -1;
                                Console.WriteLine("Unable to retrieve 'Billing Last Name' numerical column designation. Data contained in the 'Billing First Name' will be used instead.");
                                SD.Trace.WriteLine(DateTime.Now + " Unable to retrieve 'Billing Last Name' numerical column designation. Data contained in the 'Billing First Name' will be used instead.");
                            }
                            break;

                        case "import_column_billing_address1":
                            try { columnBillToAddress1 = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Billing Address - Line 1' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_billing_address2":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnBillToAddress2 = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnBillToAddress2 = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Billing Address - Line 2' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_billing_city":
                            try { columnBillToCity = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Billing City' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_billing_state":
                            try { columnBillToState = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Billing State' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_billing_zip":
                            try { columnBillToZip = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Billing Zip' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_billing_country":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnBillToCountry = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnBillToCountry = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Billing Country' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;


                        case "import_column_billing_phone":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnBillToPhone = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnBillToPhone = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Billing Phone' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_billing_email":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnBillToEmail = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnBillToEmail = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Billing E-mail' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_billing_company":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnBillToCompany = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnBillToCompany = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Billing Company Name' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_shipping_firstname":
                            try { columnShipToFirstName = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Shipping First Name' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_shipping_lastname":
                            try { columnShipToLastName = System.Convert.ToInt32(node.InnerText); }
                            catch
                            {
                                columnShipToLastName = -1;
                                Console.WriteLine("Unable to retrieve 'Shipping Last Name' numerical column designation. Data contained in the 'Shipping First Name' will be used instead.");
                                SD.Trace.WriteLine(DateTime.Now + " Unable to retrieve 'Shipping Last Name' numerical column designation. Data contained in the 'Shipping First Name' will be used instead.");
                            }
                            break;

                        case "import_column_shipping_address1":
                            try { columnShipToAddress1 = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Shipping Address - Line 1' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_shipping_address2":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipToAddress2 = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipToAddress2 = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Shipping Address - Line 2' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_shipping_city":
                            try { columnShipToCity = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Shipping City' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_shipping_state":
                            try { columnShipToState = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Shipping State' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_shipping_zip":
                            try { columnShipToZip = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Shipping Zip' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_shipping_country":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipToCountry = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipToCountry = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Shipping Country' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;


                        case "import_column_shipping_phone":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipToPhone = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipToPhone = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Shipping Phone Number' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_shipping_email":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipToEmail = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipToEmail = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Shipping E-mail' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_shipping_company":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnShipToCompany = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnShipToCompany = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Shipping Company Name' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_item_linenumber":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnItemLineNumber = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnItemLineNumber = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Line Item Number' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_item_id":

                            try { columnItemID = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Item ID' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_item_desc":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnItemDescription = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnItemDescription = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Item Description' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_item_custom1":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnItemCustom1 = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnItemCustom1 = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Position 1' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_item_custom2":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnItemCustom2 = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnItemCustom2 = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Position 2' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_item_custom3":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnItemCustom3 = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnItemCustom3 = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Position 3' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_item_price":

                            try { columnItemUnitPrice = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Item Price' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                        case "import_column_item_tax":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnItemTax = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnItemTax = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Item Tax' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_item_shipcharge":
                            try
                            {
                                xmlValueCheck = node.InnerText;

                                if (xmlValueCheck.Trim() != String.Empty) { columnItemShippingCharge = System.Convert.ToInt32(xmlValueCheck); }
                                else { columnItemShippingCharge = -1; }
                            }
                            catch { throw new Exception("Unable to retrieve 'Item Shipping Charge' numerical column designation. Please make sure the value is a numerical character. This column is optional."); }
                            break;

                        case "import_column_item_qty":
                            try { columnItemQuantity = System.Convert.ToInt32(node.InnerText); }
                            catch { throw new Exception("Unable to retrieve 'Item Quantity' numerical column designation. Please make sure the value is a numerical character. This column is required."); }
                            break;

                    }
                    #endregion
                }

 
            }
            catch (Exception ex)
            {
                HandleException("One or more elements in column transformation settings were not found. " + ex.Message, ex.StackTrace, true, true);
            }

        }



        // 4. Load data from files and convert to XML for storing in database
        public void LoadImportFileData(FileInfo importFile)
        {
            Console.WriteLine("Loading data from import file");
            string currentImportFilePath;

            try
            {
                currentImportFileInfo = importFile;
                currentImportFilePath = currentImportFileInfo.DirectoryName + "\\" + currentImportFileInfo.Name;

                if (currentImportFileInfo.Extension.ToLower() == ".xml")
                {
                    LoadRawDataXml(currentImportFilePath);
                }
                else
                {
                    LoadRawDataText(currentImportFilePath);
                }

                Console.WriteLine("Preparing data from import file");

                PrepareXmlFileData();

                SaveXmlFileData(xmlFileDataDoc, archiveXmlPath, currentImportFileInfo.Name.Replace(".xml", "") + " [FileData_" + String.Format("{0:yyyyMMdd_Hmms}", DateTime.Now) + "].xml");
            }
            catch (Exception ex)
            {
                HandleException("Error occurred when loading data from import file. Exception Type: " + ex.GetType() + ". Message: " + ex.Message.ToString(), ex.StackTrace.ToString(), true, true);
            }

        }

        // Load raw data from XML file
        private void LoadRawDataXml(string filePath)
        {
            try
            {
                xmlLoadedDataDoc.Load(filePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // Load raw data from text (CSV) files
        private void LoadRawDataText(string filePath)
        {

            try
            {

                string pattern = string.Empty;

                // Specify regex pattern for allowing commas between quote marks if commas are a delimiting character for the file.
                if (incomingColDelimiter == ",")
                {
                    pattern = ",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))";
                }
                else
                {
                    pattern = "[" + incomingColDelimiter + "]";
                }

                System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(pattern);

                // Open the file and read its contents
                using (StreamReader sr = new StreamReader(filePath))
                {

                    // Read the first line and split it into array to create data table columns
                    string fileRow = sr.ReadLine();
                    string[] columns = rx.Split(fileRow);

                    // Create table and columns if this is the first file. If not, clear it as the following files in this job will re-use it.
                    if (!dsImportFile.Tables.Contains("LoadedData"))
                    {
                        dsImportFile.Tables.Add("LoadedData");

                        if (incomingFileHeaders == true)
                        {
                            // Add values from the first line as header names
                            foreach (string col in columns)
                            {
                                dsImportFile.Tables["LoadedData"].Columns.Add(col);
                            }
                        }
                        else
                        {
                            // Add numerical values as header names
                            for (int i = 0; i < columns.Length; i++)
                            {
                                dsImportFile.Tables["LoadedData"].Columns.Add(i.ToString());
                            }
                        }
                    }
                    else
                    {
                        dsImportFile.Tables["LoadedData"].Clear();
                    }

                    // Add data contained within the first row that already has been read if it does not contain headers
                    if (incomingFileHeaders == false) { dsImportFile.Tables["LoadedData"].Rows.Add(columns); }

                    // Add data from remaining rows
                    string remainingData = sr.ReadToEnd();
                    string[] rows = remainingData.Split(incomingRowDelimiter.ToCharArray());

                    foreach (string r in rows)
                    {
                        if (r.Length != 0)
                        {
                            string[] item = rx.Split(r);
                            dsImportFile.Tables["LoadedData"].Rows.Add(item);
                        }
                    }

                    Console.WriteLine("Loaded " + dsImportFile.Tables["LoadedData"].Rows.Count.ToString() + " row(s) from import file");
                    SD.Trace.WriteLine(DateTime.Now + " Loaded " + dsImportFile.Tables["LoadedData"].Rows.Count.ToString() + " row(s) from import file");
                }

            }
            catch (Exception ex)
            {
                HandleException(ex.Message.ToString(), ex.StackTrace.ToString(), true, true);
            }

        }

        // Prepare XML of raw data for storing as a reference
        private void PrepareXmlFileData()
        {

            try
            {

                int rowCounter = 0;
                XmlElement xmlRoot = xmlFileDataDoc.CreateElement("import");
                
                XmlElement xmlFile = xmlFileDataDoc.CreateElement("file");
                XmlAttribute xmlFileName = xmlFileDataDoc.CreateAttribute("file_name");
                XmlAttribute xmlFileSize = xmlFileDataDoc.CreateAttribute("file_size");
                XmlAttribute xmlFileCreateDate = xmlFileDataDoc.CreateAttribute("file_create_date");
                XmlAttribute xmlFileModifyDate = xmlFileDataDoc.CreateAttribute("file_modify_date");
                XmlAttribute xmlFileRows = xmlFileDataDoc.CreateAttribute("data_rows");
                XmlAttribute xmlFileColumns = xmlFileDataDoc.CreateAttribute("data_columns");

                xmlFileName.Value = currentImportFileInfo.Name;
                xmlFileSize.Value = currentImportFileInfo.Length.ToString();
                xmlFileCreateDate.Value = currentImportFileInfo.CreationTime.ToString();
                xmlFileModifyDate.Value = currentImportFileInfo.LastWriteTime.ToString();

                if (currentImportFileInfo.Extension.ToLower() != ".xml")
                {
                    xmlFileRows.Value = dsImportFile.Tables["LoadedData"].Rows.Count.ToString();
                    xmlFileColumns.Value = dsImportFile.Tables["LoadedData"].Columns.Count.ToString();
                }
                else
                {
                    xmlFileRows.Value = "0";
                    xmlFileColumns.Value = "0";
                }

                xmlFile.SetAttributeNode(xmlFileName);
                xmlFile.SetAttributeNode(xmlFileSize);
                xmlFile.SetAttributeNode(xmlFileCreateDate);
                xmlFile.SetAttributeNode(xmlFileModifyDate);
                xmlFile.SetAttributeNode(xmlFileRows);
                xmlFile.SetAttributeNode(xmlFileColumns);

                xmlRoot.AppendChild(xmlFile);

                if (currentImportFileInfo.Extension.ToLower() != ".xml")
                {
                    // Store contents of the file, if applicable
                    if (flagStoreFileContents)
                    {
                        // Loop through each row and create data representation in XML form
                        foreach (System.Data.DataRow r in dsImportFile.Tables["LoadedData"].Rows)
                        {
                            rowCounter++;
                            XmlElement xmlDataRow = xmlFileDataDoc.CreateElement("data_row");
                            XmlAttribute xmlRowNumber = xmlFileDataDoc.CreateAttribute("row_no");
                            XmlAttribute xmlExternalId = xmlFileDataDoc.CreateAttribute("external_id");
                            XmlAttribute xmlCustomerOrderNo = xmlFileDataDoc.CreateAttribute("customer_order_no");
                            XmlAttribute xmlExternalOrderFlag = xmlFileDataDoc.CreateAttribute("external_order_flag");
                            XmlAttribute xmlFirstName = xmlFileDataDoc.CreateAttribute("first_name");
                            XmlAttribute xmlLastName = xmlFileDataDoc.CreateAttribute("last_name");
                            XmlAttribute xmlLineItemNo = xmlFileDataDoc.CreateAttribute("line_item_no");
                            XmlAttribute xmlLineItemId = xmlFileDataDoc.CreateAttribute("line_item_id");

                            xmlRowNumber.Value = rowCounter.ToString();
                            xmlExternalId.Value = ValidateDataItem(columnOrderExternalId, r);
                            xmlCustomerOrderNo.Value = ValidateDataItem(columnCustomerOrderNo, r);
                            xmlExternalOrderFlag.Value = ValidateDataItem(columnExternalOrderFlag, r);
                            xmlFirstName.Value = ValidateDataItem(columnBillToFirstName, r);
                            xmlLastName.Value = ValidateDataItem(columnBillToLastName, r);
                            xmlLineItemNo.Value = ValidateDataItem(columnItemLineNumber, r);
                            xmlLineItemId.Value = ValidateDataItem(columnItemID, r);

                            xmlDataRow.SetAttributeNode(xmlRowNumber);
                            xmlDataRow.SetAttributeNode(xmlExternalId);
                            xmlDataRow.SetAttributeNode(xmlCustomerOrderNo);
                            xmlDataRow.SetAttributeNode(xmlExternalOrderFlag);
                            xmlDataRow.SetAttributeNode(xmlFirstName);
                            xmlDataRow.SetAttributeNode(xmlLastName);
                            xmlDataRow.SetAttributeNode(xmlLineItemNo);
                            xmlDataRow.SetAttributeNode(xmlLineItemId);

                            xmlFile.AppendChild(xmlDataRow);
                        }
                    }
                }
                else
                {
                    // Store contents of the file, if applicable
                    if (flagStoreFileContents)
                    {
                        XmlNodeList itemXmlList;
                        XmlNode rawXmlDataRoot, orderNode, orderBillingNode;

                        rawXmlDataRoot = xmlLoadedDataDoc.DocumentElement;
                        itemXmlList = rawXmlDataRoot.SelectNodes("//order/item");

                        foreach (XmlNode itemNode in itemXmlList)
                        {

                            rowCounter++;
                            orderNode = itemNode.ParentNode;
                            orderBillingNode = orderNode["billing_address"];

                            XmlElement xmlDataRow = xmlFileDataDoc.CreateElement("data_row");
                            XmlAttribute xmlRowNumber = xmlFileDataDoc.CreateAttribute("row_no");
                            XmlAttribute xmlExternalId = xmlFileDataDoc.CreateAttribute("external_id");
                            XmlAttribute xmlCustomerOrderNo = xmlFileDataDoc.CreateAttribute("customer_order_no");
                            XmlAttribute xmlExternalOrderFlag = xmlFileDataDoc.CreateAttribute("external_order_flag");
                            XmlAttribute xmlFirstName = xmlFileDataDoc.CreateAttribute("first_name");
                            XmlAttribute xmlLastName = xmlFileDataDoc.CreateAttribute("last_name");
                            XmlAttribute xmlLineItemNo = xmlFileDataDoc.CreateAttribute("line_item_no");
                            XmlAttribute xmlLineItemId = xmlFileDataDoc.CreateAttribute("line_item_id");

                            xmlRowNumber.Value = rowCounter.ToString();

                            xmlExternalId.Value = orderNode.Attributes["external_id"].Value;

                            if (orderNode.Attributes["external_order_flag"] != null)
                            {
                                xmlExternalOrderFlag.Value = orderNode.Attributes["external_order_flag"].Value;
                            }

                            if (orderNode.Attributes["customer_order_no"] != null)
                            {
                                xmlCustomerOrderNo.Value = orderNode.Attributes["customer_order_no"].Value;
                            }

                            xmlFirstName.Value = orderBillingNode.Attributes["billing_first_name"].Value;
                            xmlLastName.Value = orderBillingNode.Attributes["billing_last_name"].Value;
                            xmlLineItemNo.Value = itemNode.Attributes["item_linenumber"].Value;
                            xmlLineItemId.Value = itemNode.Attributes["item_id"].Value;

                            xmlDataRow.SetAttributeNode(xmlRowNumber);
                            xmlDataRow.SetAttributeNode(xmlExternalId);
                            xmlDataRow.SetAttributeNode(xmlCustomerOrderNo);
                            xmlDataRow.SetAttributeNode(xmlExternalOrderFlag);
                            xmlDataRow.SetAttributeNode(xmlFirstName);
                            xmlDataRow.SetAttributeNode(xmlLastName);
                            xmlDataRow.SetAttributeNode(xmlLineItemNo);
                            xmlDataRow.SetAttributeNode(xmlLineItemId);

                            xmlFile.AppendChild(xmlDataRow);
                        }
                    }

                }

                xmlFileDataDoc.AppendChild(xmlRoot);

                if (!flagStoreFileContents)
                {
                    Console.WriteLine("File contents will not be stored due to session settings ('store_file_contents_flag' is false)");
                    SD.Trace.WriteLine(DateTime.Now + " File contents will not be stored due to session settings ('store_file_contents_flag' is false)");
                }

            }
            catch(Exception ex)
            {
                HandleException(ex.Message, ex.StackTrace, true, true);
            }

        }

        // Save created XML representation
        private void SaveXmlFileData(XmlDocument xmlDoc, string filePath, string fileName)
        {
            try
            {
                XmlTextWriter xmlFileWriter = new XmlTextWriter(filePath + fileName, null);
                xmlFileWriter.Formatting = Formatting.Indented;
                xmlDoc.Save(xmlFileWriter);
                xmlFileWriter.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // 5. Store original file data in database
        public void StoreImportFileData()
        {

            Console.WriteLine("Storing import file data in database");

            try
            {
                DataAccess dbTask = new DataAccess();
                DataSet dsResults = new DataSet();

                dsResults = dbTask.ProcessXmlFileDataWithResults(storeId, databaseConnectionString, "dbo.sproc_FileImport_XMLSourceImport", xmlFileDataDoc);

                importFileId = System.Convert.ToInt32(dsResults.Tables[0].Rows[0]["file_import_id"].ToString());

                Console.WriteLine("Import file data has been successfully stored in database");
                SD.Trace.WriteLine(DateTime.Now + " Import file data has been successfully stored in database");
            }
            catch (Exception ex)
            {
                HandleException("SQL exception occurred when storing file data. Exception Type: " + ex.GetType() + ". Message: " + ex.Message.ToString(), ex.StackTrace.ToString(), true, true);
            }


        }



        // 6. Transform data according to transformation settings (CSV only)
        // 7. Convert dataset to XML (CSV only)
        public void TransformImportFileData()
        {
            
            try
            {
                if (flagImportFileContents)
                {

                    if (currentImportFileInfo.Extension != ".xml")
                    {
                        Console.WriteLine("Transforming order file data");
                        TransformTextFileFileData();
                    }

                    PrepareXmlImportData();
                }
                else
                {
                    Console.WriteLine("File contents will not be transformed due to session settings ('import_file_contents_flag' is false)");
                    SD.Trace.WriteLine(DateTime.Now + " File contents will not be transformed due to session settings ('import_file_contents_flag' is false)");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // Prepares target datatable for transformed data
        private void PrepareTransformTable()
        {
            dsImportFile.Tables.Add("TransformedData");

            dsImportFile.Tables["TransformedData"].Columns.Add("external_id", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_date", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_status", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_type", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("external_order_type", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("external_order_flag", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("customer_order_no", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_referral_id", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("project_number", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_source", typeof(string));

            dsImportFile.Tables["TransformedData"].Columns.Add("grand_total", typeof(decimal));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_shipping", typeof(decimal));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_handling", typeof(decimal));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_sales_tax", typeof(decimal));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_sales_tax_id", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_sales_tax_county", typeof(string));

            dsImportFile.Tables["TransformedData"].Columns.Add("requested_ship_method", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("need_date", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("order_reason", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_instructions", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_comment", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("warehouse_comment", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("cost_center", typeof(string));

            dsImportFile.Tables["TransformedData"].Columns.Add("third_party_shipper_flag", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("third_party_shipper_info", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_invoice_template_id", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_attn_to", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("custom_message", typeof(string));

            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_firstname", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_lastname", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_address1", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_address2", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_city", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_state", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_zip", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_country", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_phone", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_email", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("bill_to_company", typeof(string));

            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_firstname", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_lastname", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_address1", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_address2", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_city", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_state", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_zip", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_country", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_phone", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_email", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("ship_to_company", typeof(string));

            dsImportFile.Tables["TransformedData"].Columns.Add("line_item_number", typeof(int));
            dsImportFile.Tables["TransformedData"].Columns.Add("item_id", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("item_description", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("custom1", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("custom2", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("custom3", typeof(string));
            dsImportFile.Tables["TransformedData"].Columns.Add("unit_price", typeof(decimal));
            dsImportFile.Tables["TransformedData"].Columns.Add("item_tax", typeof(decimal));
            dsImportFile.Tables["TransformedData"].Columns.Add("item_shipping_charge", typeof(decimal));
            dsImportFile.Tables["TransformedData"].Columns.Add("quantity", typeof(int));
            dsImportFile.Tables["TransformedData"].Columns.Add("line_item_qty", typeof(int));

        }

        // Transform orders data from source format to standard import format
        private void TransformTextFileFileData()
        {

            try
            {

                if (dsImportFile.Tables.Contains("TransformedData"))
                {
                    dsImportFile.Tables["TransformedData"].Clear();
                }
                else
                {
                    PrepareTransformTable();
                }

                // Prepare sort order for the table
                string sortOrder = dsImportFile.Tables["LoadedData"].Columns[columnOrderExternalId].ColumnName.ToString();

                if (columnItemLineNumber > 0)
                {
                    sortOrder += "," + dsImportFile.Tables["LoadedData"].Columns[columnItemLineNumber].ColumnName.ToString();
                }

                // Pre-sort the table to assure line item are in correct order
                DataRow[] sortedRows = dsImportFile.Tables["LoadedData"].Select(null, sortOrder);

                string currentOrderId = "";
                string lastOrderId = "";
                int currentLineItem = 1;

                // Cycle through existing data and store transform it
                foreach (DataRow r in sortedRows)
                {

                    DataRow dr = dsImportFile.Tables["TransformedData"].NewRow();

                    // Skip rows that have a blank Order ID
                    if (r[columnOrderExternalId].ToString() != String.Empty)
                    {

                        currentOrderId = ValidateDataItem(columnOrderExternalId, r);

                        dr["external_id"] = currentOrderId;

                        // If order date is blank or cannot be successfully converted, use current time/date
                        try
                        {
                            if (ValidateDataItem(columnOrderDate, r) == String.Empty) { dr["order_date"] = DateTime.Now; }
                            else { dr["order_date"] = System.Convert.ToDateTime(ValidateDataItem(columnOrderDate, r)); }
                        }
                        catch
                        {
                            dr["order_date"] = DateTime.Now;
                            Console.WriteLine("Unable to convert '" + ValidateDataItem(columnOrderDate, r) + "' to a valid DateTime format in order " + currentOrderId + ". Using current date/time instead.");
                        }

                        if (ValidateDataItem(columnOrderStatus, r) == String.Empty)
                        {
                            dr["order_status"] = "O";
                        }
                        else { dr["order_status"] = ValidateDataItem(columnOrderStatus, r); }

                        if (ValidateDataItem(columnOrderType, r) == String.Empty)
                        {
                            dr["order_type"] = "public";
                        }
                        else { dr["order_type"] = ValidateDataItem(columnOrderType, r); }

                        dr["external_order_type"] = ValidateDataItem(columnExternalOrderType, r);
                        dr["external_order_flag"] = ValidateDataItem(columnExternalOrderFlag, r);
                        dr["customer_order_no"] = ValidateDataItem(columnCustomerOrderNo, r);
                        dr["order_referral_id"] = ValidateDataItem(columnReferralId, r);
                        dr["project_number"] = ValidateDataItem(columnProjectNo, r);

                        if (ValidateDataItem(columnOrderSource, r) == String.Empty)
                        {
                            dr["order_source"] = "File Import";
                        }
                        else { dr["order_source"] = ValidateDataItem(columnOrderSource, r); }

                        if (ValidateDataItem(columnOrderTotal, r) == String.Empty) { dr["grand_total"] = 0; }
                        else { dr["grand_total"] = System.Convert.ToDecimal(ValidateDataItem(columnOrderTotal, r)); }

                        if (ValidateDataItem(columnOrderShippingCharge, r) == String.Empty) { dr["order_shipping"] = 0; }
                        else { dr["order_shipping"] = System.Convert.ToDecimal(ValidateDataItem(columnOrderShippingCharge, r)); }

                        if (ValidateDataItem(columnOrderHandlingCharge, r) == String.Empty) { dr["order_handling"] = 0; }
                        else { dr["order_handling"] = System.Convert.ToDecimal(ValidateDataItem(columnOrderHandlingCharge, r)); }

                        if (ValidateDataItem(columnSalesTax, r) == String.Empty) { dr["order_sales_tax"] = 0; }
                        else { dr["order_sales_tax"] = System.Convert.ToDecimal(ValidateDataItem(columnSalesTax, r)); }

                        dr["order_sales_tax_id"] = ValidateDataItem(columnSalesTaxId, r);
                        dr["order_sales_tax_county"] = ValidateDataItem(columnSalesTaxCounty, r);

                        dr["requested_ship_method"] = ValidateDataItem(columnShipMethod, r);
                        dr["need_date"] = ValidateDataItem(columnNeedDate, r);
                        dr["order_reason"] = ValidateDataItem(columnOrderReason, r);
                        dr["ship_instructions"] = ValidateDataItem(columnShipInstructions, r);
                        dr["ship_comment"] = ValidateDataItem(columnShipLabel, r);
                        dr["warehouse_comment"] = ValidateDataItem(columnWhseLabel, r);
                        dr["cost_center"] = ValidateDataItem(columnCostCenter, r);

                        dr["third_party_shipper_flag"] = ValidateDataItem(columnThirdPartyShipFlag, r);
                        dr["third_party_shipper_info"] = ValidateDataItem(columnThirdPatryShipInfo, r);
                        dr["ship_invoice_template_id"] = ValidateDataItem(columnShipInvoiceTemplate, r);
                        dr["ship_attn_to"] = ValidateDataItem(columnShipAttnTo, r);
                        dr["custom_message"] = ValidateDataItem(columnCustomMessage, r);

                        // If 'Billing Last Name' column does not exist it means that the names are merged into first name and need to be split
                        if (columnBillToLastName == -1)
                        {
                            string jointName = ValidateDataItem(columnBillToFirstName, r);

                            // If there are no spaces, use blank for the last name
                            if (jointName.Contains(" "))
                            {
                                string[] splitName = jointName.Split(new Char[] { ' ' }, 2);
                                dr["bill_to_firstname"] = splitName[0];
                                dr["bill_to_lastname"] = splitName[1];
                            }
                            else
                            {
                                dr["bill_to_firstname"] = jointName;
                                dr["bill_to_lastname"] = " ";
                            }
                        }
                        else
                        {
                            dr["bill_to_firstname"] = ValidateDataItem(columnBillToFirstName, r);
                            dr["bill_to_lastname"] = ValidateDataItem(columnBillToLastName, r);
                        }

                        dr["bill_to_address1"] = ValidateDataItem(columnBillToAddress1, r);
                        dr["bill_to_address2"] = ValidateDataItem(columnBillToAddress2, r);
                        dr["bill_to_city"] = ValidateDataItem(columnBillToCity, r);
                        dr["bill_to_state"] = ValidateDataItem(columnBillToState, r);
                        dr["bill_to_zip"] = ValidateDataItem(columnBillToZip, r);
                        dr["bill_to_country"] = ValidateDataItem(columnBillToCountry, r);
                        dr["bill_to_phone"] = ValidateDataItem(columnBillToPhone, r);
                        dr["bill_to_email"] = ValidateDataItem(columnBillToEmail, r);
                        dr["bill_to_company"] = ValidateDataItem(columnBillToCompany, r);

                        // If 'Shipping Last Name' column does not exist it means that the names are merged into first name and need to be split
                        if (columnShipToLastName == -1)
                        {
                            string jointName = ValidateDataItem(columnShipToFirstName, r);

                            // If there are no spaces, use blank for the last name
                            if (jointName.Contains(" "))
                            {
                                string[] splitName = jointName.Split(new Char[] { ' ' }, 2);
                                dr["ship_to_firstname"] = splitName[0];
                                dr["ship_to_lastname"] = splitName[1];
                            }
                            else
                            {
                                dr["ship_to_firstname"] = jointName;
                                dr["ship_to_lastname"] = " ";
                            }
                        }
                        else
                        {
                            dr["ship_to_firstname"] = ValidateDataItem(columnShipToFirstName, r);
                            dr["ship_to_lastname"] = ValidateDataItem(columnShipToLastName, r);
                        }

                        dr["ship_to_address1"] = ValidateDataItem(columnShipToAddress1, r);
                        dr["ship_to_address2"] = ValidateDataItem(columnShipToAddress2, r);
                        dr["ship_to_city"] = ValidateDataItem(columnShipToCity, r);
                        dr["ship_to_state"] = ValidateDataItem(columnShipToState, r);
                        dr["ship_to_zip"] = ValidateDataItem(columnShipToZip, r);
                        dr["ship_to_country"] = ValidateDataItem(columnShipToCountry, r);
                        dr["ship_to_phone"] = ValidateDataItem(columnShipToPhone, r);
                        dr["ship_to_email"] = ValidateDataItem(columnShipToEmail, r);
                        dr["ship_to_company"] = ValidateDataItem(columnShipToCompany, r);

                        if (ValidateDataItem(columnItemLineNumber, r) == String.Empty)
                        {
                            if (currentOrderId == lastOrderId) { currentLineItem++; }
                            else { currentLineItem = 1; }
                            dr["line_item_number"] = currentLineItem;
                            lastOrderId = currentOrderId;
                        }
                        else { dr["line_item_number"] = System.Convert.ToInt32(ValidateDataItem(columnItemLineNumber, r)); }

                        dr["item_id"] = ValidateDataItem(columnItemID, r);
                        dr["item_description"] = ValidateDataItem(columnItemDescription, r);
                        dr["custom1"] = ValidateDataItem(columnItemCustom1, r);
                        dr["custom2"] = ValidateDataItem(columnItemCustom2, r);
                        dr["custom3"] = ValidateDataItem(columnItemCustom3, r);

                        // Obtain unit price
                        if (ValidateDataItem(columnItemUnitPrice, r) == String.Empty)
                        {
                            dr["unit_price"] = 0;
                        }
                        else
                        {

                            // Adjustment for MindJet: perform reverse calculation from quantity and total line item price for single-user products (multi-user products have always qty of 1)
                            if (storeId == 273)
                            {
                                if (ValidateDataItem(columnItemDescription, r).IndexOf(@"Multi") > -1)
                                {
                                    dr["unit_price"] = System.Convert.ToDecimal(ValidateDataItem(columnItemUnitPrice, r));
                                }
                                else
                                {
                                    dr["unit_price"] = System.Convert.ToDecimal(ValidateDataItem(columnItemUnitPrice, r)) / System.Convert.ToDecimal(ValidateDataItem(columnItemQuantity, r));
                                }
                            }
                            else
                            {
                                dr["unit_price"] = System.Convert.ToDecimal(ValidateDataItem(columnItemUnitPrice, r));
                            }

                        }

                        // Obtain item tax
                        if (ValidateDataItem(columnItemTax, r) == String.Empty)
                        {
                            dr["item_tax"] = 0;
                        }
                        else
                        {
                            dr["item_tax"] = System.Convert.ToDecimal(ValidateDataItem(columnItemTax, r));
                        }

                        // Obtain item shipping charge
                        if (ValidateDataItem(columnItemShippingCharge, r) == String.Empty)
                        {
                            dr["item_shipping_charge"] = 0;
                        }
                        else
                        {
                            dr["item_shipping_charge"] = System.Convert.ToDecimal(ValidateDataItem(columnItemShippingCharge, r));
                        }
                        
                        int quantity = 0;
                        bool result = System.Int32.TryParse(ValidateDataItem(columnItemQuantity, r), out quantity);

                        if (result)
                        {
                            dr["quantity"] = quantity;
                        }
                        else
                        {
                            // Exception for SocialThinking
                            decimal qtyDecimal = System.Convert.ToDecimal(ValidateDataItem(columnItemQuantity, r).Trim());
                            dr["quantity"] = System.Convert.ToInt32(qtyDecimal);
                        }

                        dsImportFile.Tables["TransformedData"].Rows.Add(dr);

                    }

                }

                SD.Trace.WriteLine(DateTime.Now + " Transformed " + dsImportFile.Tables["TransformedData"].Rows.Count.ToString() + " data rows");

            }
            catch (Exception ex)
            {
                HandleException("Unable to complete data transformation. Exception Type: " + ex.GetType() + ". Message: " + ex.Message.ToString(), ex.StackTrace.ToString(), true, true);
            }


        }

        // Prepare transformed XML text file data for verification
        private void PrepareXmlImportData()
        {

            try
            {

                if (currentImportFileInfo.Extension.ToLower() == ".xml")
                {
                    xmlImportDataDoc = xmlLoadedDataDoc;
                }
                else
                {

                    string orderNumber = "";
                    XmlElement xmlRoot = xmlImportDataDoc.CreateElement("import");

                    // Loop through each transformed row and create orders and associated line items in XML form
                    foreach (System.Data.DataRow dRow in dsImportFile.Tables["TransformedData"].Rows)
                    {
                        if (orderNumber != dRow["external_id"].ToString())
                        {

                            #region "Create order and its attributes"
                            XmlElement xmlOrder = xmlImportDataDoc.CreateElement("order");
                            XmlAttribute xmlExternalId = xmlImportDataDoc.CreateAttribute("external_id");
                            XmlAttribute xmlOrderDate = xmlImportDataDoc.CreateAttribute("order_date");
                            XmlAttribute xmlInternalStatus = xmlImportDataDoc.CreateAttribute("order_status");
                            XmlAttribute xmlInternalOrderType = xmlImportDataDoc.CreateAttribute("order_type");
                            XmlAttribute xmlExternalOrderType = xmlImportDataDoc.CreateAttribute("external_order_type");
                            XmlAttribute xmlExternalOrderFlag = xmlImportDataDoc.CreateAttribute("external_order_flag");
                            XmlAttribute xmlCustomerNo = xmlImportDataDoc.CreateAttribute("customer_order_no");
                            XmlAttribute xmlRefSourceId = xmlImportDataDoc.CreateAttribute("order_referral_id");
                            XmlAttribute xmlProjectNumber = xmlImportDataDoc.CreateAttribute("project_number");
                            XmlAttribute xmlOrderSource = xmlImportDataDoc.CreateAttribute("order_source");
                            XmlAttribute xmlGrandTotal = xmlImportDataDoc.CreateAttribute("grand_total");
                            XmlAttribute xmlShipping = xmlImportDataDoc.CreateAttribute("shipping");
                            XmlAttribute xmlHandling = xmlImportDataDoc.CreateAttribute("handling");
                            XmlAttribute xmlSalesTax = xmlImportDataDoc.CreateAttribute("order_sales_tax");
                            XmlAttribute xmlSalesTaxId = xmlImportDataDoc.CreateAttribute("order_sales_tax_id");
                            XmlAttribute xmlSalesTaxCounty = xmlImportDataDoc.CreateAttribute("order_sales_tax_county");
                            XmlAttribute xmlShipMethod = xmlImportDataDoc.CreateAttribute("requested_ship_method");
                            XmlAttribute xmlNeedDate = xmlImportDataDoc.CreateAttribute("need_date");
                            XmlAttribute xmlOrderReason = xmlImportDataDoc.CreateAttribute("order_reason");
                            XmlAttribute xmlShipComment = xmlImportDataDoc.CreateAttribute("ship_comment");
                            XmlAttribute xmlWhseComment = xmlImportDataDoc.CreateAttribute("warehouse_comment");
                            XmlAttribute xmlCostCenter = xmlImportDataDoc.CreateAttribute("cost_center");
                            XmlAttribute xmlThirdPartyShipper = xmlImportDataDoc.CreateAttribute("third_party_shipper_flag");
                            XmlAttribute xmlThirdPartyShipInfo = xmlImportDataDoc.CreateAttribute("third_party_shipper_info");
                            XmlAttribute xmlShipInvoiceTemplate = xmlImportDataDoc.CreateAttribute("ship_invoice_template_id");
                            XmlAttribute xmlShipAttnTo = xmlImportDataDoc.CreateAttribute("ship_attn_to");
                            XmlAttribute xmlCustomMessage = xmlImportDataDoc.CreateAttribute("custom_message");
                            XmlAttribute xmlLineItemCount = xmlImportDataDoc.CreateAttribute("line_item_qty");

                            xmlExternalId.Value = dRow["external_id"].ToString();
                            xmlOrderDate.Value = dRow["order_date"].ToString();
                            xmlInternalStatus.Value = dRow["order_status"].ToString();
                            xmlInternalOrderType.Value = dRow["order_type"].ToString();
                            xmlExternalOrderType.Value = dRow["external_order_type"].ToString();
                            xmlExternalOrderFlag.Value = dRow["external_order_flag"].ToString();
                            xmlCustomerNo.Value = dRow["customer_order_no"].ToString();
                            xmlRefSourceId.Value = dRow["order_referral_id"].ToString();
                            xmlProjectNumber.Value = dRow["project_number"].ToString();
                            xmlOrderSource.Value = dRow["order_source"].ToString();
                            xmlGrandTotal.Value = dRow["grand_total"].ToString();
                            xmlShipping.Value = dRow["order_shipping"].ToString();
                            xmlHandling.Value = dRow["order_handling"].ToString();
                            xmlSalesTax.Value = dRow["order_sales_tax"].ToString();
                            xmlSalesTaxId.Value = dRow["order_sales_tax_id"].ToString();
                            xmlSalesTaxCounty.Value = dRow["order_sales_tax_county"].ToString();
                            xmlShipMethod.Value = dRow["requested_ship_method"].ToString();
                            xmlNeedDate.Value = dRow["need_date"].ToString();
                            xmlOrderReason.Value = dRow["order_reason"].ToString();
                            xmlShipComment.Value = dRow["ship_comment"].ToString() + " " + dRow["ship_instructions"];
                            xmlWhseComment.Value = dRow["warehouse_comment"].ToString();
                            xmlCostCenter.Value = dRow["cost_center"].ToString();
                            xmlThirdPartyShipper.Value = dRow["third_party_shipper_flag"].ToString();
                            xmlThirdPartyShipInfo.Value = dRow["third_party_shipper_info"].ToString();
                            xmlShipInvoiceTemplate.Value = dRow["ship_invoice_template_id"].ToString();
                            xmlShipAttnTo.Value = dRow["ship_attn_to"].ToString();
                            xmlCustomMessage.Value = dRow["custom_message"].ToString();
                            xmlLineItemCount.Value = dRow["line_item_qty"].ToString();

                            xmlOrder.SetAttributeNode(xmlExternalId);
                            xmlOrder.SetAttributeNode(xmlOrderDate);
                            xmlOrder.SetAttributeNode(xmlInternalStatus);
                            xmlOrder.SetAttributeNode(xmlInternalOrderType);
                            xmlOrder.SetAttributeNode(xmlExternalOrderType);
                            xmlOrder.SetAttributeNode(xmlExternalOrderFlag);
                            xmlOrder.SetAttributeNode(xmlCustomerNo);
                            xmlOrder.SetAttributeNode(xmlRefSourceId);
                            xmlOrder.SetAttributeNode(xmlProjectNumber);
                            xmlOrder.SetAttributeNode(xmlOrderSource);
                            xmlOrder.SetAttributeNode(xmlGrandTotal);
                            xmlOrder.SetAttributeNode(xmlShipping);
                            xmlOrder.SetAttributeNode(xmlHandling);
                            xmlOrder.SetAttributeNode(xmlSalesTax);
                            xmlOrder.SetAttributeNode(xmlSalesTaxId);
                            xmlOrder.SetAttributeNode(xmlSalesTaxCounty);
                            xmlOrder.SetAttributeNode(xmlShipMethod);
                            xmlOrder.SetAttributeNode(xmlNeedDate);
                            xmlOrder.SetAttributeNode(xmlOrderReason);
                            xmlOrder.SetAttributeNode(xmlShipComment);
                            xmlOrder.SetAttributeNode(xmlWhseComment);
                            xmlOrder.SetAttributeNode(xmlCostCenter);
                            xmlOrder.SetAttributeNode(xmlThirdPartyShipper);
                            xmlOrder.SetAttributeNode(xmlThirdPartyShipInfo);
                            xmlOrder.SetAttributeNode(xmlShipInvoiceTemplate);
                            xmlOrder.SetAttributeNode(xmlShipAttnTo);
                            xmlOrder.SetAttributeNode(xmlCustomMessage);
                            xmlOrder.SetAttributeNode(xmlLineItemCount);

                            xmlRoot.AppendChild(xmlOrder);
                            #endregion

                            #region "Create billing address"
                            XmlElement xmlBillingAddress = xmlImportDataDoc.CreateElement("billing_address");

                            XmlAttribute xmlBillingFirstName = xmlImportDataDoc.CreateAttribute("billing_first_name");
                            XmlAttribute xmlBillingLastName = xmlImportDataDoc.CreateAttribute("billing_last_name");
                            XmlAttribute xmlBillingAddress1 = xmlImportDataDoc.CreateAttribute("billing_address1");
                            XmlAttribute xmlBillingAddress2 = xmlImportDataDoc.CreateAttribute("billing_address2");
                            XmlAttribute xmlBillingCity = xmlImportDataDoc.CreateAttribute("billing_city");
                            XmlAttribute xmlBillingState = xmlImportDataDoc.CreateAttribute("billing_state");
                            XmlAttribute xmlBillingZip = xmlImportDataDoc.CreateAttribute("billing_zip");
                            XmlAttribute xmlBillingCountry = xmlImportDataDoc.CreateAttribute("billing_country");
                            XmlAttribute xmlBillingPhone = xmlImportDataDoc.CreateAttribute("billing_phone");
                            XmlAttribute xmlBillingEmail = xmlImportDataDoc.CreateAttribute("billing_email");
                            XmlAttribute xmlBillingCompany = xmlImportDataDoc.CreateAttribute("billing_company");

                            xmlBillingFirstName.Value = dRow["bill_to_firstname"].ToString();
                            xmlBillingLastName.Value = dRow["bill_to_lastname"].ToString();
                            xmlBillingAddress1.Value = dRow["bill_to_address1"].ToString();
                            xmlBillingAddress2.Value = dRow["bill_to_address2"].ToString();
                            xmlBillingCity.Value = dRow["bill_to_city"].ToString();
                            xmlBillingState.Value = dRow["bill_to_state"].ToString();
                            xmlBillingZip.Value = dRow["bill_to_zip"].ToString();
                            xmlBillingCountry.Value = dRow["bill_to_country"].ToString();
                            xmlBillingPhone.Value = dRow["bill_to_phone"].ToString();
                            xmlBillingEmail.Value = dRow["bill_to_email"].ToString();
                            xmlBillingCompany.Value = dRow["bill_to_company"].ToString();

                            xmlBillingAddress.SetAttributeNode(xmlBillingFirstName);
                            xmlBillingAddress.SetAttributeNode(xmlBillingLastName);
                            xmlBillingAddress.SetAttributeNode(xmlBillingAddress1);
                            xmlBillingAddress.SetAttributeNode(xmlBillingAddress2);
                            xmlBillingAddress.SetAttributeNode(xmlBillingCity);
                            xmlBillingAddress.SetAttributeNode(xmlBillingState);
                            xmlBillingAddress.SetAttributeNode(xmlBillingZip);
                            xmlBillingAddress.SetAttributeNode(xmlBillingCountry);
                            xmlBillingAddress.SetAttributeNode(xmlBillingPhone);
                            xmlBillingAddress.SetAttributeNode(xmlBillingEmail);
                            xmlBillingAddress.SetAttributeNode(xmlBillingCompany);

                            xmlOrder.AppendChild(xmlBillingAddress);
                            #endregion

                            #region "Create shipping address"
                            XmlElement xmlShippingAddress = xmlImportDataDoc.CreateElement("shipping_address");

                            XmlAttribute xmlShippingFirstName = xmlImportDataDoc.CreateAttribute("shipping_first_name");
                            XmlAttribute xmlShippingLastName = xmlImportDataDoc.CreateAttribute("shipping_last_name");
                            XmlAttribute xmlShippingAddress1 = xmlImportDataDoc.CreateAttribute("shipping_address1");
                            XmlAttribute xmlShippingAddress2 = xmlImportDataDoc.CreateAttribute("shipping_address2");
                            XmlAttribute xmlShippingCity = xmlImportDataDoc.CreateAttribute("shipping_city");
                            XmlAttribute xmlShippingState = xmlImportDataDoc.CreateAttribute("shipping_state");
                            XmlAttribute xmlShippingZip = xmlImportDataDoc.CreateAttribute("shipping_zip");
                            XmlAttribute xmlShippingCountry = xmlImportDataDoc.CreateAttribute("shipping_country");
                            XmlAttribute xmlShippingPhone = xmlImportDataDoc.CreateAttribute("shipping_phone");
                            XmlAttribute xmlShippingEmail = xmlImportDataDoc.CreateAttribute("shipping_email");
                            XmlAttribute xmlShippingCompany = xmlImportDataDoc.CreateAttribute("shipping_company");

                            xmlShippingFirstName.Value = dRow["ship_to_firstname"].ToString();
                            xmlShippingLastName.Value = dRow["ship_to_lastname"].ToString();
                            xmlShippingAddress1.Value = dRow["ship_to_address1"].ToString();
                            xmlShippingAddress2.Value = dRow["ship_to_address2"].ToString();
                            xmlShippingCity.Value = dRow["ship_to_city"].ToString();
                            xmlShippingState.Value = dRow["ship_to_state"].ToString();
                            xmlShippingZip.Value = dRow["ship_to_zip"].ToString();
                            xmlShippingCountry.Value = dRow["ship_to_country"].ToString();
                            xmlShippingPhone.Value = dRow["ship_to_phone"].ToString();
                            xmlShippingEmail.Value = dRow["ship_to_email"].ToString();
                            xmlShippingCompany.Value = dRow["ship_to_company"].ToString();

                            xmlShippingAddress.SetAttributeNode(xmlShippingFirstName);
                            xmlShippingAddress.SetAttributeNode(xmlShippingLastName);
                            xmlShippingAddress.SetAttributeNode(xmlShippingAddress1);
                            xmlShippingAddress.SetAttributeNode(xmlShippingAddress2);
                            xmlShippingAddress.SetAttributeNode(xmlShippingCity);
                            xmlShippingAddress.SetAttributeNode(xmlShippingState);
                            xmlShippingAddress.SetAttributeNode(xmlShippingZip);
                            xmlShippingAddress.SetAttributeNode(xmlShippingCountry);
                            xmlShippingAddress.SetAttributeNode(xmlShippingPhone);
                            xmlShippingAddress.SetAttributeNode(xmlShippingEmail);
                            xmlShippingAddress.SetAttributeNode(xmlShippingCompany);

                            xmlOrder.AppendChild(xmlShippingAddress);

                            xmlImportDataDoc.AppendChild(xmlRoot);

                            orderNumber = dRow["external_id"].ToString();
                            #endregion
                        }

                        #region "Append line items to the existing order"
                        XmlNode xmlOrderNode = xmlImportDataDoc.SelectSingleNode("//order[@external_id='" + orderNumber + "']");

                        XmlElement xmlItem = xmlImportDataDoc.CreateElement("item");

                        XmlAttribute itemLineNo = xmlImportDataDoc.CreateAttribute("item_linenumber");
                        XmlAttribute itemId = xmlImportDataDoc.CreateAttribute("item_id");
                        XmlAttribute itemDesc = xmlImportDataDoc.CreateAttribute("item_desc");
                        XmlAttribute itemCustom1 = xmlImportDataDoc.CreateAttribute("item_custom1");
                        XmlAttribute itemCustom2 = xmlImportDataDoc.CreateAttribute("item_custom2");
                        XmlAttribute itemCustom3 = xmlImportDataDoc.CreateAttribute("item_custom3");
                        XmlAttribute itemPrice = xmlImportDataDoc.CreateAttribute("item_price");
                        XmlAttribute itemTax = xmlImportDataDoc.CreateAttribute("item_tax");
                        XmlAttribute itemShipCharge = xmlImportDataDoc.CreateAttribute("item_shipcharge");
                        XmlAttribute itemQty = xmlImportDataDoc.CreateAttribute("item_qty");

                        itemLineNo.Value = dRow["line_item_number"].ToString();
                        itemId.Value = dRow["item_id"].ToString();
                        itemDesc.Value = dRow["item_description"].ToString();
                        itemCustom1.Value = dRow["custom1"].ToString();
                        itemCustom2.Value = dRow["custom2"].ToString();
                        itemCustom3.Value = dRow["custom3"].ToString();
                        itemPrice.Value = dRow["unit_price"].ToString();
                        itemTax.Value = dRow["item_tax"].ToString();
                        itemShipCharge.Value = dRow["item_shipping_charge"].ToString();
                        itemQty.Value = dRow["quantity"].ToString();

                        xmlItem.SetAttributeNode(itemLineNo);
                        xmlItem.SetAttributeNode(itemId);
                        xmlItem.SetAttributeNode(itemDesc);
                        xmlItem.SetAttributeNode(itemCustom1);
                        xmlItem.SetAttributeNode(itemCustom2);
                        xmlItem.SetAttributeNode(itemCustom3);
                        xmlItem.SetAttributeNode(itemPrice);
                        xmlItem.SetAttributeNode(itemTax);
                        xmlItem.SetAttributeNode(itemShipCharge);
                        xmlItem.SetAttributeNode(itemQty);

                        xmlOrderNode.AppendChild(xmlItem);
                        xmlImportDataDoc.AppendChild(xmlRoot);
                        #endregion

                    }
                }


                // Update order dates depending on the configuration setting (use dates provided in file vs. current date/time of the import agent
                if (flagKeepOrderDate == false)
                {
                    XmlNodeList orderXmlList;
                    XmlNode rawXmlRoot;
                    rawXmlRoot = xmlImportDataDoc.DocumentElement;
                    orderXmlList = rawXmlRoot.SelectNodes("//order");

                    foreach (XmlNode orderNode in orderXmlList)
                    {
                        orderNode.Attributes["order_date"].Value = System.DateTime.Now.ToString();
                    }
                }


            }
            catch (Exception ex)
            {
                HandleException("Error occurred during data conversion to XML. " + ex.Message.ToString(), ex.StackTrace.ToString(), true, true);
            }

        }



        // 8. Verify data in XML, checking for invalid or incomplete orders and invalid items, 
        public void VerifyImportFileData()
        {

            try
            {
                bool flagValidationErrors = false;

                if (flagImportFileContents)
                {
                    flagValidationErrors = ValidateOrders();
                }
                else
                {
                    Console.WriteLine("File data will not be validated due to session settings ('import_file_contents_flag' is false)");
                    SD.Trace.WriteLine(DateTime.Now + "File data will not be validated due to session settings ('import_file_contents_flag' is false)");
                }

                if (flagValidationErrors)
                {
                    RemoveInvalidOrders();
                    PrepareXmlValidationErrorData();
                    SaveXmlFileData(xmlFileValidationErrorDoc, archiveXmlPath, currentImportFileInfo.Name.Replace(".xml", "") + " [ValidationErrors_" + String.Format("{0:yyyyMMdd_HHmms}", DateTime.Now) + "].xml");
                    StoreValidationErrorData();
                }

                if (archiveXmlPath != String.Empty && flagXmlArchive)
                {
                    SaveXmlFileData(xmlImportDataDoc, archiveXmlPath, currentImportFileInfo.Name.Replace(".xml", "") + " [ImportData_" + String.Format("{0:yyyyMMdd_HHmms}", DateTime.Now) + "].xml");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // Perform validation of orders and items before import
        private bool ValidateOrders()
        {

            try
            {

                XmlNodeList orderXmlList, itemXmlList;
                XmlNode rawXmlRoot;

                int rowCounter, itemLineId, lineItemId, itemLineCount;
                string orderId, itemId, validationError, externalOrderFlag;
                bool flagInvalidOrder, flagInvalidItem, flagValidationErrors;

                itemLineCount = 0;
                flagValidImportFile = true;
                flagValidationErrors = false;
                rawXmlRoot = xmlImportDataDoc.DocumentElement;
                orderXmlList = rawXmlRoot.SelectNodes("//order");

                // Validate each order
                foreach (XmlNode orderNode in orderXmlList)
                {
                    
                    orderId = orderNode.Attributes["external_id"].Value;
                    
                    if (orderNode.Attributes["external_order_flag"] != null) { externalOrderFlag = orderNode.Attributes["external_order_flag"].Value; }
                    else { externalOrderFlag = "0"; }

                    if (orderNode.Attributes["line_item_qty"] != null)
                    {
                        if (orderNode.Attributes["line_item_qty"].Value == String.Empty) { itemLineCount = 0; }
                        else { itemLineCount = System.Convert.ToInt32(orderNode.Attributes["line_item_qty"].Value); }
                    }
                    else 
                    {
                        itemLineCount = 0; 
                    }

                    flagInvalidOrder = false;
                    flagInvalidItem = false;
                    rowCounter = 0;
                    itemLineId = 0;
                    validationError = String.Empty;
                    lineItemId = 0;
                    itemId = String.Empty;
                    itemXmlList = orderNode.ChildNodes;

                    // Validate each line item/item ID
                    foreach (XmlNode itemNode in itemXmlList)
                    {
                        if (itemNode.Name == "item")
                        {
                            rowCounter++;
                            itemId = itemNode.Attributes["item_id"].Value;
                            lineItemId = System.Convert.ToInt32(itemNode.Attributes["item_linenumber"].Value);

                            if (itemLineId < lineItemId)
                            {
                                itemLineId = lineItemId;
                            }

                            // Check whether line item is valid [true] or not or inactive [false]
                            if (IsValidDbData(storeId, "ITEM", itemId, externalOrderFlag) == false)
                            {
                                flagInvalidItem = true;
                                validationError += "\nItem '" + itemId + "' is inactive or does not exist in the database.";
                                dsUnprocessedOrders.Tables["Orders"].Rows.Add(orderId, lineItemId.ToString(), itemId, "INVALID_ITEM_IN_DB", "Item is inactive or does not exist in the database");
                            }
                        }
                    }

                    // Check whether this order is unique [true] (or a duplicate [false])
                    if (IsValidDbData(storeId, "ORDER", orderId, externalOrderFlag) == false)
                    {
                        flagInvalidOrder = true;
                        validationError += "\nOrder ID (external_id) already exists in the database or has been resolved as invalid.";
                        dsUnprocessedOrders.Tables["Orders"].Rows.Add(orderId, "0", "0", "ID_EXISTS_IN_DB", "Order ID (external_id) already exists in the database or has been resolved as invalid");
                    }

                    // Check whether file contains multiple instances of the same order
                    if (rawXmlRoot.SelectNodes("//order[@external_id='" + orderId + "']").Count > 1)
                    {
                        flagInvalidOrder = true;
                        validationError += "\nMultiple instances of the same order ID exist within the file";
                        dsUnprocessedOrders.Tables["Orders"].Rows.Add(orderId, "0", "0", "ID_EXISTS_IN_FILE", "Multiple instances of the same order ID exist within the file");
                    }

                    /*
                    XmlNode nodeBillingAddress = rawXmlRoot.SelectNodes("//order[@external_id='" + orderId + "']/billing_address")[0];
                    XmlNode nodeShippingAddress = rawXmlRoot.SelectNodes("//order[@external_id='" + orderId + "']/shipping_address")[0];
                    */

                    // Check whether number of line items matches (only if this validation is requested - otherwise, line item ID )
                    if ((rowCounter != itemLineId) && flagValidateLineItems == true)
                    {
                        flagInvalidOrder = true;
                        validationError += "\nLine item count (" + rowCounter + ") does not match highest item line number (" + itemLineId + ")";
                        dsUnprocessedOrders.Tables["Orders"].Rows.Add(orderId, "0", "0", "INVALID_LINE_ITEM_COUNT", "Line item count (" + rowCounter + ") does not match highest item line number (" + itemLineId + ")");
                    }

                    // Check whether number of line items matches line_item_qty (only if one is explicitly provided)
                    if ((rowCounter != itemLineCount) && itemLineCount != 0)
                    {
                        flagInvalidOrder = true;
                        validationError += "\nLine item count (" + rowCounter + ") does not match line item quantity specified in the order (" + itemLineCount + ")";
                        dsUnprocessedOrders.Tables["Orders"].Rows.Add(orderId, "0", "0", "INVALID_LINE_ITEM_COUNT", "Line item count (" + rowCounter + ") does not match line item quantity specified in the order (" + itemLineCount + ")");
                    }

                    /*
                    // Check whether billing address is provided
                    if (nodeBillingAddress.Attributes["billing_address1"].Value == String.Empty || nodeBillingAddress.Attributes["billing_city"].Value == String.Empty || nodeBillingAddress.Attributes["billing_zip"].Value == String.Empty || nodeBillingAddress.Attributes["billing_country"].Value == String.Empty)
                    {
                        flagInvalidOrder = true;
                        validationError += "\nBilling address is missing one or more elements.";
                        dsUnprocessedOrders.Tables["Orders"].Rows.Add(orderId, "0", "0", "INCOMPLETE_BILLING_ADDRESS", "Billing address is missing one or more elements.");
                    }

                    // Check whether shipping address is provided
                    if (nodeShippingAddress.Attributes["shipping_address1"].Value == String.Empty || nodeShippingAddress.Attributes["shipping_city"].Value == String.Empty || nodeShippingAddress.Attributes["shipping_zip"].Value == String.Empty || nodeShippingAddress.Attributes["shipping_country"].Value == String.Empty)
                    {
                        flagInvalidOrder = true;
                        validationError += "\nShipping address is missing one or more elements.";
                        dsUnprocessedOrders.Tables["Orders"].Rows.Add(orderId, "0", "0", "INCOMPLETE_SHIPPING_ADDRESS", "Shipping address is missing one or more elements.");
                    }
                    */

                    if (flagInvalidOrder == true || flagInvalidItem == true)
                    {

                        flagValidationErrors = true;
                        validationError = validationError.Replace("\n", "\nValidation for order '" + orderId + "' has failed: ");

                        Console.WriteLine(validationError);
                        SD.Trace.WriteLine(validationError);

                        if (flagResumeOnItemError == false)
                        {
                            flagValidImportFile = false;
                        }

                        if (flagResumeOnOrderError == false)
                        {
                            flagValidImportFile = false;
                        }

                    }

                }

                return flagValidationErrors;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Validate the value of the data column
        private string ValidateDataItem(int columnID, DataRow dataImportRow)
        {

            string outputString = "";

            // If the column was not specified in the mapping or it is blank, use a default value
            if (columnID != -1)
            {
                if (dataImportRow[columnID].ToString().Trim() != String.Empty)
                {
                    outputString = System.Convert.ToString(dataImportRow[columnID]);
                }
            }

            return outputString;
        }

        // Remove all invalid orders from XML representation before import
        private void RemoveInvalidOrders()
        {
            try
            {
                foreach (DataRow row in dsUnprocessedOrders.Tables["Orders"].Rows)
                {
                    XmlNode rawXmlRoot = xmlImportDataDoc.DocumentElement;
                    XmlNodeList orderXmlList = rawXmlRoot.SelectNodes("//order[@external_id='" + row["OrderID"].ToString() + "']");

                    foreach (XmlNode orderNode in orderXmlList)
                    {
                        orderNode.ParentNode.RemoveChild(orderNode);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        // Check whether orders or items in the databaseare valid
        private bool IsValidDbData(int storeId, string valueType, string valueCheck, string valueParameter)
        {
            bool dbResult = false;
            string nameStoredProcedure = String.Empty;

            DataAccess dbTask = new DataAccess();

            switch (valueType)
            {
                case "ORDER":
                    nameStoredProcedure = sqlSPValidateOrder;
                    break;

                case "ITEM":
                    nameStoredProcedure = sqlSPValidateItem;
                    break;
            }

            dbResult = dbTask.CheckDbValue(databaseConnectionString, nameStoredProcedure, storeId, valueCheck, valueParameter);

            return dbResult;
        }

        // Prepare XML representation of errors encountered during validation for storing with file data in database
        private void PrepareXmlValidationErrorData()
        {
            try
            {

                XmlDocument xmlDoc = new XmlDocument();
                XmlElement xmlRoot = xmlDoc.CreateElement("import");

                foreach (DataRow row in dsUnprocessedOrders.Tables["Orders"].Rows)
                {
                    XmlElement xmlDataRow = xmlDoc.CreateElement("data_row");
                    XmlAttribute xmlImportFileId = xmlDoc.CreateAttribute("file_import_id");
                    XmlAttribute xmlExternalId = xmlDoc.CreateAttribute("external_id");
                    XmlAttribute xmlLineItemNo = xmlDoc.CreateAttribute("item_linenumber");
                    XmlAttribute xmlLineId = xmlDoc.CreateAttribute("item_id");
                    XmlAttribute xmlErrorCode = xmlDoc.CreateAttribute("error_code");
                    XmlAttribute xmlErrorDesc = xmlDoc.CreateAttribute("error_desc");

                    xmlImportFileId.Value = importFileId.ToString();
                    xmlExternalId.Value = row["OrderID"].ToString();
                    xmlLineItemNo.Value = row["LineItemNo"].ToString();
                    xmlLineId.Value = row["ItemID"].ToString();
                    xmlErrorCode.Value = row["ErrorCode"].ToString();
                    xmlErrorDesc.Value = row["ErrorDesc"].ToString();

                    xmlDataRow.SetAttributeNode(xmlImportFileId);
                    xmlDataRow.SetAttributeNode(xmlExternalId);
                    xmlDataRow.SetAttributeNode(xmlLineItemNo);
                    xmlDataRow.SetAttributeNode(xmlLineId);
                    xmlDataRow.SetAttributeNode(xmlErrorCode);
                    xmlDataRow.SetAttributeNode(xmlErrorDesc);

                    xmlRoot.AppendChild(xmlDataRow);

                }

                xmlDoc.AppendChild(xmlRoot);                
                xmlFileValidationErrorDoc = xmlDoc;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // Update existing file data with validation error information
        private void StoreValidationErrorData()
        {
            try
            {

                Console.WriteLine("Storing validation errors in database");

                try
                {
                    DataAccess dbTask = new DataAccess();
                    dbTask.ProcessXmlFileData(storeId, databaseConnectionString, "dbo.sproc_FileImport_XMLErrorUpdate", xmlFileValidationErrorDoc);

                    Console.WriteLine("Validation errors have been successfully stored in database");
                    SD.Trace.WriteLine(DateTime.Now + " Validation errors have been successfully stored in database");
                }
                catch (Exception ex)
                {
                    HandleException("SQL exception occurred when storing validation errors in database. Exception Type: " + ex.GetType() + ". Message: " + ex.Message.ToString(), ex.StackTrace.ToString(), true, true);
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        // 9. Export data to SQL server for bulk data processing by a stored procedure
        public void ImportFileData()
        {
            try
            {
                if (flagImportFileContents)
                {
                    if (flagValidImportFile && xmlImportDataDoc.DocumentElement.ChildNodes.Count != 0)
                    {
                        Console.WriteLine("Importing data into database");

                        DataAccess dbTask = new DataAccess();

                        dsProcessedOrders = dbTask.ProcessXmlFileDataWithResults(storeId, databaseConnectionString, "dbo.sproc_FileImport_XMLOrderImport", xmlImportDataDoc);

                        Console.WriteLine("Orders from data file have been successfully imported to database");
                        SD.Trace.WriteLine(DateTime.Now + " Orders from data file have been successfully imported to database");
                    }
                    else
                    {
                        Console.WriteLine("Orders from data file will not be imported to database because there are no valid orders");
                        SD.Trace.WriteLine(DateTime.Now + " Orders from data file will not be imported because there are no valid orders");
                    }
                }
                else
                {
                    Console.WriteLine("Orders from data file will not be imported due to session settings ('import_file_contents_flag' is false)");
                    SD.Trace.WriteLine(DateTime.Now + " Orders from data file will not be imported due to session settings ('import_file_contents_flag' is false)");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // 10. Get final operation details and send operation e-mails, if necessary
        public void FinishImport()
        {

            
            if (flagEmailOrderConfirmation)
            {
                try
                {
                    // Send order confirmation to billing address e-mails
                    SendOrderConfirmationMail();
                    Console.WriteLine("Finished sending order confirmation e-mails for file '" + currentImportFileInfo.Name + "'");
                    SD.Trace.WriteLine(DateTime.Now + " Finished sending order confirmation e-mails for file '" + currentImportFileInfo.Name + "'");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed sending order confirmation e-mail. " + ex.Message + ex.StackTrace);
                    SD.Trace.WriteLine(DateTime.Now + " Failed sending order confirmation e-mail. " + ex.Message + ex.StackTrace);
                    throw ex;
                }
            }

            
            if (flagEmailAdminConfirmation)
            {
                try
                {
                    // Send file import summary notification
                    SendImportConfirmationMail(currentImportFileInfo.Name);

                    Console.WriteLine("Import summary notification e-mail sent successfuly");
                    SD.Trace.WriteLine(DateTime.Now + " Import summary notification e-mail sent successfuly");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed sending import summary notification e-mail. " + ex.Message + ex.StackTrace);
                    SD.Trace.WriteLine(DateTime.Now + " Failed sending import summary notification e-mail. " + ex.Message + ex.StackTrace);
                    throw ex;
                }
            }

            // Archive import file so it won't be processed again
            ArchiveFile();

            // Clean up XML files
            xmlLoadedDataDoc.RemoveAll();
            xmlFileDataDoc.RemoveAll();
            xmlFileValidationErrorDoc.RemoveAll();
            xmlImportDataDoc.RemoveAll();
            
        }


        // 11. Send reorder notifications
        public void ProcessReorderNotifications()
        {

            if (flagEmailReorderNotification)
            {

                try
                {

                    if (dsProcessedOrders.Tables.Count > 0)
                    {
                        DataRow[] orderRows = dsProcessedOrders.Tables[2].Select();

                        if (orderRows.Length <= 0)
                        {
                            Console.WriteLine("Reorder points were not reached for any of the items");
                            SD.Trace.WriteLine(DateTime.Now + "Reorder points were not reached for any of the items");
                        }

                        foreach (DataRow r in orderRows)
                        {
                            Console.WriteLine("Reorder point has been reached for : " + r["item_id"].ToString() + " (Reorder Point: " + r["reorder_point"].ToString() + ". Quantity Imported: " + r["total_qty_ordered"].ToString() + ". Qty Available Before Import: " + r["qty_available_before"].ToString() + ". Qty Available After Import: " + r["qty_available_after"].ToString() + ")");
                            SD.Trace.WriteLine(DateTime.Now + "Reorder point has been reached for : " + r["item_id"].ToString() + " (Reorder Point: " + r["reorder_point"].ToString() + ". Quantity Imported: " + r["total_qty_ordered"].ToString() + ". Qty Available Before Import: " + r["qty_available_before"].ToString() + ". Qty Available After Import: " + r["qty_available_after"].ToString() + ")");

                            if (storeId == 380)
                            {
                                SendReorderNotificationMail(r["item_id"].ToString(), r["prod_name"].ToString(), r["reorder_point"].ToString(), r["qty_available_before"].ToString(), r["qty_available_after"].ToString(), r["total_qty_ordered"].ToString());
                            }
                            else
                            {
                                SendReorderNotificationMail(r["item_id"].ToString(), null, r["reorder_point"].ToString(), r["qty_available_before"].ToString(), r["qty_available_after"].ToString(), r["total_qty_ordered"].ToString());
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Reorder points were not reached for any of the items");
                        SD.Trace.WriteLine(DateTime.Now + "Reorder points were not reached for any of the items");
                    }

                }
                catch (Exception ex)
                {
                    HandleException("Error occurred during processing Reorder Notifications: " + ex.Message, ex.StackTrace, true, false);
                }

            }

        }

        // Send individual order confirmations
        private void SendOrderConfirmationMail()
        {

            try
            {

                Mailer emailMessage = new Mailer();

                DataRow[] processedOrders = dsProcessedOrders.Tables[0].Select();
                DataRow[] processedLineItems;
                string emailConfirmSubject;
                string emailConfirmBody;
                string lineItemList;
                string lineItemBreakChar;
                bool emailHTMLEncoding;

                foreach (DataRow dr in processedOrders)
                {

                    emailConfirmSubject = String.Empty;
                    emailConfirmBody = String.Empty;
                    lineItemList = String.Empty;
                    emailHTMLEncoding = false;

                    if (emailOrderConfirmBody.Contains(".html"))
                    {
                        lineItemBreakChar = "<br>";
                        emailHTMLEncoding = true;
                    }
                    else
                    {
                        lineItemBreakChar = "\r\f";
                        emailHTMLEncoding = false;
                    }

                    emailConfirmSubject = emailOrderConfirmSubject;
                    emailConfirmBody = LoadEmailBody(emailOrderConfirmBody);

                    // Update e-mail subject
                    emailConfirmSubject = emailConfirmSubject.Replace("[%ORDER_ID%]", dr["order_id"].ToString());
                    emailConfirmSubject = emailConfirmSubject.Replace("[%ORDER_SUFFIX%]", dr["order_suffix"].ToString());
                    emailConfirmSubject = emailConfirmSubject.Replace("[%EXTERNAL_ORDER_ID%]", dr["external_order_id"].ToString());

                    // Create item list
                    processedLineItems = dsProcessedOrders.Tables[1].Select("order_id = '" + dr["order_id"].ToString() + "'", "line_no");

                    foreach (DataRow dr2 in processedLineItems)
                    {
                        lineItemList += lineItemBreakChar + dr2["line_no"].ToString() + ". " + dr2["item_id"].ToString() + " " + dr2["item_name"].ToString() + "\t  Quantity: " + dr2["qty_ordered"].ToString();
                    }

                    // Update e-mail body
                    emailConfirmBody = emailConfirmBody.Replace("[%ORDER_LINE_ITEMS%]", lineItemList);

                    emailConfirmBody = emailConfirmBody.Replace("[%ORDER_ID%]", dr["order_id"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%ORDER_SUFFIX%]", dr["order_suffix"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%EXTERNAL_ORDER_ID%]", dr["external_order_id"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%ORDER_DATE%]", dr["order_when"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%LINE_ITEM_COUNT%]", dr["line_count"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%ORDER_TOTAL%]", System.String.Format("{0:C}", dr["grand_total"], "$12345.00"));
                    emailConfirmBody = emailConfirmBody.Replace("[%SHIP_COMMENT%]", dr["ship_label_comment"].ToString());

                    emailConfirmBody = emailConfirmBody.Replace("[%BILLING_FIRSTNAME%]", dr["bill_first"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%BILLING_LASTNAME%]", dr["bill_last"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%BILLING_ADDRESS1%]", dr["bill_address1"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%BILLING_ADDRESS2%]", dr["bill_address2"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%BILLING_CITY%]", dr["bill_city"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%BILLING_STATE%]", dr["bill_state"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%BILLING_ZIP%]", dr["bill_zip"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%BILLING_COUNTRY%]", dr["bill_country"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%BILLING_EMAIL%]", dr["bill_email"].ToString());

                    emailConfirmBody = emailConfirmBody.Replace("[%SHIPPING_FIRSTNAME%]", dr["ship_first"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%SHIPPING_LASTNAME%]", dr["ship_last"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%SHIPPING_ADDRESS1%]", dr["ship_address1"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%SHIPPING_ADDRESS2%]", dr["ship_address2"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%SHIPPING_CITY%]", dr["ship_city"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%SHIPPING_STATE%]", dr["ship_state"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%SHIPPING_ZIP%]", dr["ship_zip"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%SHIPPING_COUNTRY%]", dr["ship_country"].ToString());
                    emailConfirmBody = emailConfirmBody.Replace("[%SHIPPING_EMAIL%]", dr["ship_email"].ToString());

                    // Add e-mail order confirmation recipients
                    emailOrderConfirmRecipientList = dr["bill_email"].ToString();

                    if (dr["bill_email"].ToString() != dr["ship_email"].ToString())
                    {
                        emailOrderConfirmRecipientList += ";" + dr["bill_email"].ToString();
                    }

                    // Send e-mail
                    emailMessage.SendMail(mailServerAddress, emailOrderConfirmFromAddress, emailOrderConfirmRecipientList, emailOrderConfirmCCList, emailOrderConfirmBCCList, emailConfirmSubject, emailOrderConfirmPriority, emailConfirmBody, emailHTMLEncoding, archiveEmailPath, flagArchiveEmail);

                    if (emailMessage.EmailSent)
                    {
                        Console.WriteLine("Confirmation e-mail with subject '" + emailConfirmSubject + "' has been successfully sent.");
                        SD.Trace.WriteLine(DateTime.Now + " Confirmation e-mail with subject '" + emailConfirmSubject + "' has been successfully sent.");
                    }
                    else
                    {
                        Console.WriteLine("Confirmation e-mail with subject '" + emailConfirmSubject + "' has not been sent.");
                        SD.Trace.WriteLine(DateTime.Now + " Confirmation e-mail with subject '" + emailConfirmSubject + "' has not been sent.");
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // Send order import summary notification
        private void SendImportConfirmationMail(string fileName)
        {

            Mailer emailMessage = new Mailer();
            string emailConfirmBody = "";
            string lineItemList = "";
            string lineItemBreakChar = "";
            bool emailHTMLEncoding = false;

            // Change E-mail Confirmation Notification settings to use Incomplete E-mail settings
            if (dsUnprocessedOrders.Tables.Count > 0)
            {
                DataRow[] orderRows = dsUnprocessedOrders.Tables[0].Select();

                if (orderRows.Length > 0)
                {
                    emailAdminConfirmFromAddress = emailAdminIncompleteFromAddress;
                    emailAdminConfirmSubject = emailAdminIncompleteSubject;
                    emailAdminConfirmBody = emailAdminIncompleteBody;
                    emailAdminConfirmRecipientList = emailAdminIncompleteRecipientList;
                    emailAdminConfirmCCList = emailAdminIncompleteCCList;
                    emailAdminConfirmBCCList = emailAdminIncompleteCCList;
                    emailAdminConfirmPriority = emailAdminIncompletePriority;                 
                }

            }

            emailConfirmBody = emailAdminConfirmBody;

            if (emailConfirmBody.Contains(".html"))
            {
                lineItemBreakChar = "<br>";
                emailHTMLEncoding = true;
            }
            else
            {
                lineItemBreakChar = "\r\f";
                emailHTMLEncoding = false;
            }

            emailConfirmBody = LoadEmailBody(emailConfirmBody);

            // Add filename to e-mail body
            emailConfirmBody = emailConfirmBody.Replace("[%FILENAME%]", fileName);

            // Add processed orders to e-mail body
            if (dsProcessedOrders.Tables.Count > 0)
            {
                DataRow[] orderRows = dsProcessedOrders.Tables[0].Select();

                if (orderRows.Length <= 0)
                {
                    lineItemList = "There were no processed orders";
                }

                foreach (DataRow r in orderRows)
                {
                    lineItemList += r["external_order_id"].ToString() + " (" + r["order_id"].ToString() + "-" + r["order_suffix"].ToString() + ")";
                    lineItemList += lineItemBreakChar;
                }
            }
            else
            {
                lineItemList = "There were no processed orders";
            }

            emailConfirmBody = emailConfirmBody.Replace("[%PROCESSED_ORDERS%]", lineItemList);

            if (flagArchiveEmail)
            {
                if (!String.IsNullOrEmpty(lineItemList))
                {
                    String filename = "";
                    String dt = DateTime.Now.ToString("MMddyyyyHmm");
                    String s = lineItemList.Replace("<br>", ",");

                    if (!String.IsNullOrEmpty(archiveEmailPath))
                    {
                        filename = archiveEmailPath + "processed_orders_" + dt + ".csv";
                        try
                        {
                            using (StreamWriter file = new StreamWriter(@filename.ToString(), true))
                            {
                                file.Write("{0}", s);
                            }
                        }
                        catch { }
                    }
                }
            }



            // Add orders that were not processed to e-mail body
            if (dsUnprocessedOrders.Tables.Count > 0)
            {

                lineItemList = "";

                DataRow[] orderRows = dsUnprocessedOrders.Tables["Orders"].Select();

                if (orderRows.Length <= 0)
                {
                    lineItemList = "There were no unprocessed orders";
                }

                foreach (DataRow r in orderRows)
                {

                    lineItemList += r["OrderID"].ToString() + " - ";
 
                    if (r["ItemID"].ToString().Trim() != "0")
                    {
                        lineItemList += r["ErrorDesc"].ToString() + " (" + r["ItemID"].ToString() + ")";
                        lineItemList += lineItemBreakChar;
                    }
                    else 
                    {
                        lineItemList += r["ErrorDesc"].ToString();
                        lineItemList += lineItemBreakChar;
                    }

                }

            }
            else
            {
                lineItemList = "There were no unprocessed orders";
            }

            emailConfirmBody = emailConfirmBody.Replace("[%UNPROCESSED_ORDERS%]", lineItemList);


            // Send e-mail
            try
            {
                emailMessage.SendMail(mailServerAddress, emailAdminConfirmFromAddress, emailAdminConfirmRecipientList, emailAdminConfirmCCList, emailAdminConfirmBCCList, emailAdminConfirmSubject, emailAdminConfirmPriority, emailConfirmBody, emailHTMLEncoding, archiveEmailPath, flagArchiveEmail);

                if (emailMessage.EmailSent)
                {
                    Console.WriteLine("Import confirmation e-mail with subject '" + emailAdminConfirmSubject + "' has been successfully sent.");
                    SD.Trace.WriteLine(DateTime.Now + " Import confirmation e-mail with subject '" + emailAdminConfirmSubject + "' has been successfully sent.");
                }
                else
                {
                    Console.WriteLine("Import confirmation e-mail with subject '" + emailAdminConfirmSubject + "' has not been sent.");
                    SD.Trace.WriteLine(DateTime.Now + " Import confirmation e-mail with subject '" + emailAdminConfirmSubject + "' has not been sent.");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // Send reorder notification
        private void SendReorderNotificationMail(string itemId, string itemDesc, string reorderPoint, string inventoryBefore, string inventoryAfter, string orderedQty)
        {

            try
            {

                Mailer emailMessage = new Mailer();

                string emailSubject;
                string emailBody;
                string lineItemList;
                //string lineItemBreakChar;
                bool emailHTMLEncoding;

                emailSubject = String.Empty;
                emailBody = String.Empty;
                lineItemList = String.Empty;
                emailHTMLEncoding = false;

                if (emailReorderNotificationBody.Contains(".html"))
                {
                    lineItemBreakChar = "<br>";
                    emailHTMLEncoding = true;
                }
                else
                {
                    lineItemBreakChar = "\r\f";
                    emailHTMLEncoding = false;
                }

                emailSubject = emailReorderNotificationSubject;
                emailBody = LoadEmailBody(emailReorderNotificationBody);

                // Update e-mail subject
                emailSubject = emailSubject.Replace("[%ITEM_ID%]", itemId);

                // Update e-mail body
                emailBody = emailBody.Replace("[%ITEM_ID%]", itemId);
                emailBody = emailBody.Replace("[%REORDER_POINT%]", reorderPoint);
                emailBody = emailBody.Replace("[%TOTAL_QTY_ORDERED%]", orderedQty);
                emailBody = emailBody.Replace("[%QTY_AVAILABLE_BEFORE%]", inventoryBefore);
                emailBody = emailBody.Replace("[%QTY_AVAILABLE_AFTER%]", inventoryAfter);

                if (!(String.IsNullOrEmpty(itemDesc)))
                {
                    emailBody = emailBody.Replace("[%ITEM_DESC%]", itemDesc);
                }

                // Send e-mail
                emailMessage.SendMail(mailServerAddress, emailReorderNotificationFromAddress, emailReorderNotificationRecipientList, emailReorderNotificationCCList, emailReorderNotificationBCCList, emailSubject, emailReorderNotificationPriority, emailBody, emailHTMLEncoding, archiveEmailPath, flagArchiveEmail);

                if (emailMessage.EmailSent)
                {
                    Console.WriteLine("Reorder notification e-mail with subject '" + emailSubject + "' has been successfully sent.");
                    SD.Trace.WriteLine(DateTime.Now + " Reorder notification e-mail with subject '" + emailSubject + "' has been successfully sent.");
                }
                else
                {
                    Console.WriteLine("Reorder notification e-mail with subject '" + emailSubject + "' has not been sent.");
                    SD.Trace.WriteLine(DateTime.Now + " Reorder notification e-mail with subject '" + emailSubject + "' has not been sent.");
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // Send administrative notification reporting any issues that occur during file import
        private void SendAdminErrorMail(string errorType, string errorTrace)
        {
            Mailer emailMessage = new Mailer();
            string emailConfirmBody = "";
            bool emailHTMLEncoding = false;

            emailConfirmBody = emailAdminErrorBody;

            if (emailConfirmBody.Contains(".html"))
            {
                emailHTMLEncoding = true;
            }
            else
            {
                emailHTMLEncoding = false;
            }

            emailConfirmBody = LoadEmailBody(emailConfirmBody);
            emailConfirmBody = emailConfirmBody.Replace("[%ERROR_TYPE%]", errorType);
            emailConfirmBody = emailConfirmBody.Replace("[%STACK_TRACE%]", errorTrace);

            try
            {
                emailMessage.SendMail(mailServerAddress, emailAdminErrorFromAddress, emailAdminErrorRecipientList, emailAdminErrorCCList, emailAdminErrorBCCList, emailAdminErrorSubject, emailAdminErrorPriority, emailConfirmBody, emailHTMLEncoding, archiveEmailPath, flagArchiveEmail);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // Load order confirmation email template
        private string LoadEmailBody(string filePath)
        {
            string emailBody = String.Empty;

            if (filePath != null)
            {
                try
                {
                    StreamReader fileTemplate = File.OpenText(filePath);
                    emailBody = fileTemplate.ReadToEnd();
                    fileTemplate.Close();

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {

                emailBody = "[%ORDER_LINE_ITEMS%]";

            }

            return emailBody;

        }

        // Archive import file, if requested, and delete it
        private void ArchiveFile()
        {

            try
            {

                if (flagArchiveFile)
                {

                    string fileName = currentImportFileInfo.Name;                    

                    if (Directory.GetFiles(archiveFilePath, fileName).Length == 0)
                    {
                        currentImportFileInfo.CopyTo(archiveFilePath + "\\" + fileName);
                        Console.WriteLine("Import file '" + fileName + "' successfully archived.");
                        SD.Trace.WriteLine(DateTime.Now + " Import file '" + fileName + "' successfully archived.");
                    }
                    else
                    {
                        currentImportFileInfo.CopyTo(archiveFilePath + "\\" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + fileName);
                        Console.WriteLine("Archive file with the same name already exists. Current import file renamed to: " + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + fileName);
                        SD.Trace.WriteLine(DateTime.Now + " Archive file with the same name already exists. Current import file renamed to: " + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + fileName);
                    }

                }

                if (!flagKeepFile)
                {
                    currentImportFileInfo.Delete();
                }

            }
            catch (Exception ex)
            {
                HandleException("Unable to archive/delete import file '" + currentImportFileInfo.Name + "'. " + ex.GetType().ToString() + " " + ex.Message.ToString(), "", false, true);
            }

            if (!flagKeepFile)
            {
                Console.WriteLine("Import file '" + currentImportFileInfo.Name + "' successfully deleted."); 
                SD.Trace.WriteLine(DateTime.Now + " Import file '" + currentImportFileInfo.Name + "' successfully deleted."); 
            }

        }

        // Handle logging, e-mailing, and rethrowing exceptions within ImportAgent.cs
        public void HandleException(string exceptionMessage, string exceptionStack, bool flagSendAdminEmail, bool flagRethrowException)
        {

            if (exceptionStack == String.Empty) { exceptionStack = "None available"; }

            // Send e-mail
            if (flagSendAdminEmail)
            {
                try
                {
                    SendAdminErrorMail(exceptionMessage, exceptionStack);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Unable to create administrative error notification e-mail. " + ex.Message.ToString());
                    SD.Trace.WriteLine(DateTime.Now + " Unable to create administrative error notification e-mail. " + ex.Message.ToString());
                }
            }

            // Rethrow exception
            if (flagRethrowException)
            {
                throw new Exception(exceptionMessage + "\nException Stack: " + exceptionStack);
            }
            else
            {
                Console.WriteLine(exceptionMessage + "\nException Stack: " + exceptionStack);
                SD.Trace.WriteLine(DateTime.Now + " " + exceptionMessage + "\nException Stack: " + exceptionStack);
            }


        }



	#endregion


	

	}
}
